#include <stdio.h>
#include <execinfo.h>
#include <stdlib.h>
#include <vector>
#include <stdint.h>
#include <sys/stat.h>
#include <iostream>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <sstream>
#include <utility>
#include <limits.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sched.h>
#include <sstream>
#include <map>
#include <mpi.h>

#include <algorithm>
#include <iostream>
#include <numeric>

#define TIME_SPENT(start, end) (end.tv_sec * 1000000. + end.tv_usec - start.tv_sec*1000000. - start.tv_usec)

const int CORESPERNODE = 24;

const int N = 1000;

const ssize_t MEMLARGE = 1024 * 1024 * 4 * 1.12; /* 1.12 for 22 procs; 0.96 for 24 is to get cpu
                                                    and mem times equal */
const ssize_t CPULARGE = 1024 * 1024 * 4;

static std::vector< float > avg_times;
static std::vector< float > std_times;
static std::vector< float > skw_times;
static std::vector< double > zresults;

static int cpuid  = -1;
static int myrank = -1;
static int commSize = -1;

static void ResetExperimentData() {
   avg_times.clear();
   std_times.clear();
   skw_times.clear();
}

static bool first = true;
static void LogExperimentData(int countmem, int countcpu) {

   if ( myrank == 0 )  {
      if (first) {
         std::cout << "legend: perc. mem : skew : stdev : avg" << std::endl;
         first = false;
      }
      std::cout << 100.*countmem/(countmem+countcpu) << " : "
                << std::accumulate(skw_times.begin(), skw_times.end(), 0.) / skw_times.size() << " : "
                << std::accumulate(std_times.begin(), std_times.end(), 0.) / std_times.size() << " : "
                << std::accumulate(avg_times.begin(), avg_times.end(), 0.) / avg_times.size()
                << std::endl;
   }
}

/*static void checkcpuid() {
   int cur_cpuid = getpid();
   assert( cur_cpuid == cpuid );
}*/

inline void collect(float ftime, float* recvbuf,
      std::vector<float>& avgs, std::vector<float>& stdevs, std::vector<float>& skews) {
   MPI_Allgather( &ftime, 1, MPI_FLOAT, recvbuf, 1, MPI_FLOAT, MPI_COMM_WORLD );

   double tavg = 0., tvar = 0., tskw = 0.;
   for ( int isum = 0; isum < commSize; ++isum )
      tavg += recvbuf[isum];
   tavg /= commSize;

   for ( int isum = 0; isum < commSize; ++isum ) {
      double diff = recvbuf[isum] - tavg;
      tvar += diff*diff;
   }

   double tsig = sqrt(tvar/commSize);
   if ( tsig != 0) {
      for ( int isum = 0; isum < commSize; ++isum ) {
         double diff = (recvbuf[isum] - tavg)/tsig;
         tskw += pow(diff, 3);
      }
   }
   tskw /= commSize;

   avgs.push_back( tavg );
   stdevs.push_back( tsig/tavg );
   skews.push_back( tskw );
}

int main( int argc, char* argv[] ) {
   MPI_Init( &argc, &argv );

   if (argc != 2) {
      std::cerr << "Usage: " << argv[0] << " <steps>" << std::endl;
      exit(1);
   }
   const ssize_t NSTEPS = atoi(argv[1]);

   char hostname[HOST_NAME_MAX];
   gethostname(hostname, HOST_NAME_MAX);

   MPI_Comm_rank( MPI_COMM_WORLD, &myrank );
   MPI_Comm_size( MPI_COMM_WORLD, &commSize );

   char* namebuf = new char[HOST_NAME_MAX*commSize];
   MPI_Allgather( hostname, HOST_NAME_MAX, MPI_CHAR,
                  namebuf,  HOST_NAME_MAX, MPI_CHAR, MPI_COMM_WORLD );
   int localCommSize = 0;
   for (int i = 0; i < commSize; ++i) {
       if (strcmp(hostname, &namebuf[i*HOST_NAME_MAX]) == 0)
          localCommSize += 1;
   }

   int* sizeCheck = NULL;
   if (myrank == 0)
      sizeCheck = new int[commSize];
   MPI_Gather(&localCommSize, 1, MPI_INT, sizeCheck, 1, MPI_INT, 0, MPI_COMM_WORLD);

   if (myrank == 0) {
      int divergers = 0;
      for (int i = 0; i < commSize; ++i) {
          if (sizeCheck[i] != sizeCheck[0]) {
             divergers += 1;
             if (sizeCheck[i] > localCommSize) localCommSize = sizeCheck[i];
          }
      }
      std::cout << "using " << localCommSize << " procs per node with " << divergers << " divergent ranks" << std::endl;
   }

   MPI_Bcast(&localCommSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
   int MEMWORKSIZE = int(MEMLARGE / double(localCommSize) * double(CORESPERNODE));

   float* recvbuf = new float[commSize];

   cpuid = sched_getcpu();

   cpu_set_t my_set;
   CPU_ZERO(&my_set);
   CPU_SET(cpuid, &my_set);
   sched_setaffinity(0, sizeof(cpu_set_t), &my_set);

   zresults.reserve( N ); 

   struct timeval t1, t2;

   for (int j = 0; j < (NSTEPS+1); j++) {
      ResetExperimentData();

      double COVER = j == 0 ? N+1 : float(NSTEPS)/j;
      double ratio = COVER - 1;

      zresults.clear();
      double x = 0., y = 1., z = 2.;
      double* work = new double[MEMWORKSIZE+1];

      int countmem = 0, countcpu = 0;
      for (int i = 0; i < N; i++) {

         //checkcpuid();

         if (i < ratio) {
         // cpu-intensive portion
            countcpu += 1;

            gettimeofday( &t1, NULL );

            for (int j = 0; j < CPULARGE; ++j ) {
               x =  1.0000001*y - z;
               y = -1.0000001*z + x;
               z =  1.0000001*x - y;
            }

            gettimeofday( &t2, NULL );
            zresults.push_back( z );

         } else {
            ratio += COVER;

         // memory-intensive portion
            countmem += 1;

            gettimeofday( &t1, NULL );

            for (int j = 0; j < MEMWORKSIZE; ++j ) {
               work[j+1] = work[j];
            }

            gettimeofday( &t2, NULL );
         }

         float ftime = float(TIME_SPENT(t1, t2));
         collect( ftime, recvbuf, avg_times, std_times, skw_times );
         MPI_Barrier( MPI_COMM_WORLD );
      }

   // use result values to prevent overly aggressive optimizations
      char buf[256];
      if ( ! zresults.empty() ) {
         snprintf(buf, 256, "%f", zresults[zresults.size() - 1]);
         setenv("OUTSIDE_EFFECT_Z", buf, 1);
      }
      snprintf(buf, 256, "%f", work[MEMWORKSIZE]);
      setenv("OUTSIDE_EFFECT_M", buf, 1);

      delete[] work;

      LogExperimentData(countmem, countcpu);

   }

   delete[] recvbuf;

   MPI_Finalize();

   return 0;
} 
