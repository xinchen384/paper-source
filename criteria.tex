

\section{Employing Variability as a Predictor}
\label{sec:criteria}

%WLAV: now show that variability per se is a workable good predictor



Fortunately, it turns out that the tables can be turned around:
timings of individual processes/events may fluctuate, but we can
predict, and make use of that variability itself.  {\it For the rest
of this study we use the term {\bf dispersion} to refer to the sample
standard deviation of all timings on all tasks executing between a
pair of group synchronization operations.}  Note that code between two
barriers is also referred to as a {\bf region}. For simplicity we will
interchange the terms barrier and collective during the
presentation. The implementation handles any flavor of operation, as
described in Section~\ref{sec:tune}.


Dispersion is induced by two main causes.  Application dispersion is
induced by domain decomposition and algorithms, which determine the
execution duration on each task. Load balanced codes, either with
static domain decomposition or dynamic load balancing, have low
dispersion. Load imbalanced codes exhibit higher dispersion. The
system itself contributes to dispersion through performance
variability and noise introduced into the execution.

We conjecture that, given a load balanced code, dispersion is caused
by the intrinsic nature of the system and the computation, when
activities compete for the same shared resource\footnote{Resources are
either hardware, e.g. memory, or software, e.g. locks inside runtime
implementations.}. Identifying these causes, determining their
variability ``signature'' and their amenability to DVFS optimizations
may allow us to build energy optimizations using a qualitative
approach without any need to predict individual timings. Our insight
is that no matter what the programmer's original intentions were, the
stochastic nature of large scale computations allows us to predict the
distribution of inefficiencies (e.g. timing of slack) in the code and
react whenever ``signature'' distributions are identified.




\parah{System induced dispersion} We use micro-benchmarks that time
code executed in between two barrier operations to understand where
and how variability appears.  We start with a statically load balanced
benchmark, where each rank performs the same work, either
communication, memory, I/O, or compute intensive. We then mix the
types of work on ranks, as well as varying the amount of load
imbalance.  We distinguish between Flops-limited (CPU-limited) and DRAM
bandwidth-limited code in the computation benchmarks, as the latter is
amenable~\cite{Deng:2012} to DVFS.

Intuitively, pure computation in SPMD codes is expected to be more
predictive and static in nature. Communication, synchronization and
I/O are prime culprits for variability, therefore prime candidates for
DVFS, since there processes compete for hardware resources.

As shown in Figure~\ref{fig:cpumemnoise} (left), dispersion in
Flops-limited code is negligible at any scale up to 2,000 cores.
Dispersion increases with DRAM pressure and inter-node communication,
ending up more than $10 \times$ larger, at any scale.
The shape of the distributions, measured with skewness and shown in
Figure~\ref{fig:cpumemnoise} (right), gives an extra distinction at
scale. Memory-limited code uses many components of the hardware: the
CPU, the hierarchy of caches, memory controllers, etc.
Stochastic behavior combined from many sources
leads to normal distributions, per the central limit theorem, and that
is what we observe: skewness is small to non-existent. In contrast,
variability in CPU-limited code comes from a single source, which
leads to an asymmetric distribution with large positive skew, caused
by a few stragglers.  CPU limited code grows a statistically
significant\footnote{Normalized by standard deviation; not in an
absolute (i.e.\ wall time) sense.} right-side tail.


We have considered all combinations of code with different
characteristics and assembled a set of micro-benchmarks to tune the
optimization engine. For brevity we omit detailed results for
communication and I/O intensive operations, we note that their
behavior is qualitatively similar to memory intensive codes when
combined.


\parah{Application induced dispersion} This is a measure of the
intrinsic application load balance, and previous work relies on its
predictability for static codes. Our insight is that ``useful''
imbalance can be recognized even for dynamically load balanced
codes. For the NWChem experiment shown as unpredictable in
Figure~\ref{fig:predictivity}, we compute the dispersion for each
region in its calling context, identified by the program stack traces.
Figure~\ref{fig:variability_predictivity} shows the results of
comparing the dispersion of the first occurrence of each context with
its subsequent occurrences, for contexts that repeat at least $10$
times.  The correlation is high and it is more likely that dispersion
increases than decreases when contexts re-occur. Clearly its presence
provides a good indicator of DVFS opportunities.

\begin{figure}[t]
\includegraphics[width=3.5in]{variability_predictivity}
\caption{\label{fig:variability_predictivity}
  \footnotesize\textit{Quality of prediction of variability per
context in NWChem, using 1024 ranks.
The dispersion of a re-occurring context is highly correlated
with that of its first occurrence, leading to
good predictivity.}}
\end{figure}

\parah{Selecting DVFS candidates} The previous results indicate that
the type of behavior amenable to DVFS (communication, I/O, memory
intensity) causes increased dispersion in ``well'' load balanced
regions. These regions can be the result of either static domain
decomposition or dynamic load balancing mechanisms. As
misclassification may hamper performance, we still need criteria to
handle the load imbalanced candidate regions.




While dispersion gives a measure of contention,
skewness gives an indication of the shape (asymmetry) of the
distribution, and thus an estimate of the behavior of the slowest
tasks (critical path).  For statically load imbalanced codes this
behavior is quantitatively predictable, while for unsuccessful dynamic load
balancing it is more or less random.


Negative skew means the mass of the distribution is concentrated
towards the right of the value range: it indicates that most tasks take
a long time, with a fat tail of fast tasks.  Intuitively, this happens when most tasks
execute on the ``critical path".  Conversely, positive skew means most
tasks are fast, with a fat tail of slow tasks, meaning that one or a
few tasks form the critical path.

Each task has some minimum duration, limiting the possible extent of
tails on the left.
When anything affects a task's execution (e.g.\ the operating system
briefly pre-empting), it will invariably lead to a slowdown compared to
the other, unaffected tasks.
This creates a small tail on the right side of the value range, the
likelihood of which is higher at scale.
As more and more tasks are affected, such as happens on an increasingly
contended resource, the distribution widens and the concentration
of tasks moves to the right of the value range.
This decreases skew, and may eventually flip it to negative.
If the contention does not affect all tasks equally, e.g.\ in
the case of a shared cache\footnote{After a first miss, a (now slower)
process faces a higher chance of seeing its cache lines evicted under any
temporal locality eviction policy}, the fat tail of stragglers continues
to grow and skew remains positive.
Thus, we want to exclude such regions, as it indicates no or uneven
contention, such as CPU- or cache-bound code, and DVFS scaling will
negatively impact performance.

Contention widens the distribution, skew is normalized to
dispersion, and there is a lower limit on task duration.
Thus unless the random variations are (much) greater than the region 
duration itself, system induced factors or poor dynamic load balancing 
will not cause a {\em large} negative skew.
Large negative skew is more likely to correspond to static imbalance with
some tasks consistently faster.
We may either exclude such regions outright or apply a histogramming
method to quickly look for bi-modal distributions.
If found, only the dispersion of the slower (right-most) mode, which
represents the tasks on the critical path, matters in principle.
However, the presence of clearly detectable modes means that the dispersion
of those sets of tasks that make up the modes is low, and that they are
thus most likely not affected by contention.

\begin{figure*}[t!]
\centering
\includegraphics[scale=0.35]{varskew.pdf}
\caption{
\footnotesize\textit{Classification by skewness and
variance. Regions using only uncontended resources, e.g.\
CPU, show low variance, whereas regions using contended resources,
such as memory, have high variance.  Flop-bound code has large
positive skew at scale, as does any code that has a significant
critical path.  This leaves the lower right corner (high variance, low
or negative skew) as most amenable to DVFS.}}
\label{fig:varskew}
\end{figure*}

Figure~\ref{fig:varskew} summarizes our strategy of applying DVFS
based on dispersion and skewness. The strategy is guided using the
micro-benchmarks already described in this Section. These are
supplemented, as described in Section~\ref{sec:tune}, with
micro-benchmarks to characterize the behavior of collective operations,
as they can be either memory, CPU, or communication intensive.
These cover all characteristics that we expect: uncontested,
symmetrically (all tasks equally) and asymmetrically contested, balanced
and imbalanced.
The goal of this mapping is to find an easily identifiable region
that indicates contention on resources and where we can thus apply DVFS
scaling.

For any observed execution
falling in {\it Tile} {\bf (e)} {\it we apply DVFS}.
The  high variance (larger
than a threshold), small positive or negative skew  are
characteristics of communication, I/O or DRAM bandwidth limited
codes.
These correspond to contention that affects all tasks in the same way.
Applying a cut on large negative skew is not needed in our target applications
and we chose not to: the slower tasks drive the timing and if these indicate
(because of their dispersion) contented resources, then applying DVFS works fine.
As outlined above, strongly statically load-imbalanced CPU-bound tasks can
exhibit a bi-modal distribution, and a more general implementation should
detect those.

For any other execution summarized by  Tiles {\bf (a)}, {\bf (b)}, {\bf (c)} and  {\bf
  (d)} {\it we do not attempt DVFS}.  For brevity, we give only the
intuition behind our decision, without any benchmark quantification.
Tile {\bf (a)}  captures flop-bound code. In this case note that  
skewness increases with concurrency. Tile {\bf (b)} captures code with
real load imbalance, but we cannot determine where, or what actual code
mix is executed.
Code in Tile {\bf (d)} is unlikely to occur in practice (and doesn't in
NWChem).
Finally, Tile {\bf (a)} captures code that executes in the last level of
shared (contended)  cache or code with a critical path.

Our approach can handle
statically load balanced and dynamically load balanced codes.  It
can also handle static load imbalanced codes provided that imbalance
is less that system induced variation. The algorithm is designed to
filter out very large static load imbalance (see
Figure~\ref{fig:adagio_varskew}). In contrast, existing work in
energy optimizations for HPC codes handles codes with large static
load imbalance.

It may  seem worthwhile to use hardware performance counters and instrument
system calls to get more detail of the programs behavior, and use that to
refine the selection.
However, we have found that logical program behaviors do not map so neatly.
For example, a polling loop while waiting for one-sided communication
to finish is fully CPU-bound, but does benefit from DVFS.
In addition, regions contain a mixture of different behaviors, which
combined with true stochastic behavior severely risks over-fitting when
too many parameters are considered.
Finally, the hardware itself and the operating system already use the
information available from performance counters and system calls to apply
DVFS, pocketing most of the energy savings accessible that way.

