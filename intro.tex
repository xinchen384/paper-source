\section{Introduction}
\label{sec:intro}

Optimizations using Dynamic Voltage and Frequency Scaling (DVFS) have
been
shown~\cite{adagio,kappiah2005sc,vishnupgas,mpicluster,etempl,hybrid}
to reduce energy usage in HPC workloads.  A rather impressive
amount of work has focused on developing energy optimizations for MPI codes,
with the main purpose of saving energy without affecting performance.
As we continue to scale systems up and out towards Exascale
performance, power becomes an important system design constraint and
thus incentives grow to deploy these optimizations in production.

This research was started as an effort to develop global control
strategies and policies for large scale HPC systems. We were
interested in developing {\it lightweight and practical} methods that
can handle highly optimized codes that use modern programming
practices, running on production systems with the latest hardware
available.

Most existing techniques~\cite{adagio,mpicluster,etempl,thrifty}
exploit the idea that the presence of {\it slack}, occurring when tasks
wait at synchronization points, indicates that DVFS can be applied on
those cores waiting for external events. Our survey of these
state-of-the-art techniques indicates that: 1)  results are
demonstrated on codes that
use static domain decomposition and exhibit  static load imbalance; 2) decisions are made
based on predictions of program running time; and
3) DVFS is always controlled per core. The current dogma states that
dynamic load balancing and runtime adaptation techniques are required
for performance on very large scale systems. Thus we are interested
in developing efficient DVFS techniques for this class of codes.
Furthermore, due to hardware or system idiosyncrasy, per core
control may not be available  or it has has limited impact when shared resources
 will not scale down unless all cores scale down.
Thus, we were interested in coarser grained DVFS control; this
functionality may also be required when the HPC community moves
from flat Single Program Multiple Data (SPMD) parallelism, e.g.\ MPI,
towards hierarchical and hybrid parallelism.


To handle dynamic program behavior, in this paper we put forth an
alternative to the existing tenet that slack is predictable.  We argue
that making quantitative predictions on timings of individual processes
in dynamic codes (parallelism or load balancing) is challenging
because of random variability. But, one can use a qualitative approach
and make use of variability itself.  Our insight, and the main paper
contribution, is that noise is intrinsic to large scale parallel
executions, that it appears whenever shared resources are contended
and that we can recognize its signature whenever code amenable to DVFS
executes. As a signature we use the combination of the dispersion
(measured as sample standard deviation) and skewness of task duration
timings between two task group synchronization points. The DVFS
amenable regions are ``dominated'' by any of: 1) blocking or
nonblocking one-sided or two-sided communication; 2) file I/O; and 3)
DRAM bandwidth limited execution.
In a production system, the first two are also affected by external
events, outside of control available within one application.

We have developed single-pass online optimizations using context
sensitive classification, as well as multi-pass offline
approaches. Note that  input and concurrency independent online
analyses~\cite{adagio,mpicluster} are required in practice for
scalability and the long term success of DVFS optimizations. 

To provide coarse grained DVFS, our algorithms were developed to
select a global system frequency assignment for  codes
using either two- or one-sided communication.  To handle the high
latency of DVFS control on production systems we use clustering
algorithms after candidate classification. We
present extensions to previous work~\cite{mpicluster}, mandatory for
efficacy on modern hardware.

As a case study we have chosen an application that poses
challenges to existing DVFS control techniques that use quantitative
predictions of execution time.  NWChem~\cite{valiev} is a framework
for complex computational chemistry that provides a range of
modules covering different theoretical models and computational
approaches.
NWChem is written using non-blocking {\it one-sided} communication (RDMA)
for overlap and communication latency hiding, augmented with {\it dynamic
load balancing} mechanisms.
Ours is the first study able to handle this class of codes. To
provide a comparison with state-of-the-art MPI centric techniques, we
use UMT2K~\cite{umt}, an unstructured mesh radiation transport code
and a common benchmark.

We validate results on three architectures: circa 2012 AMD A10-5800K
and Intel Ivy Bridge deployed in current production systems, and 2015
Intel Haswell to be installed in the new generation of very large scale
production~\cite{trinity,cori} systems. The combination of qualitative
modeling with clustering methods provides us with a programming model
and runtime independent approach.  The variability criteria can handle
both one- and two-sided communication paradigms, while timing
predictions fail for one-sided or are less efficient for
two-sided. Clustering is required in most cases for
improved performance, but needs to be done with care. The importance
of recognizing variability increases with scale.



With the online algorithm, we observe energy savings as high as 12\%
with a small speed up due to reduced congestion for one-sided
communication at high concurrency. For two-sided communication we
observe energy savings as high as 16\% with negligible slowdown.
The offline algorithm is able to  double the energy savings for
one-sided communication.
Surprisingly, this is not due to its ability to use the optimal frequency
for any region cluster in the program, but because it captures the
one-off initialization stages, which are usually dominated by disk access.
In all cases, savings are obtained where structural inefficiencies are
causing contention, without the CPU going idle enough for long enough; or
where the CPU itself is a driver of the contention.

We believe that hardware-driven scaling will soon usurp most  opportunities
for detailed software-based DVFS within a node, but coarse-grained scaling may still have
a role. Global assignments of DVFS settings have more benevolent statistics
than per-core or per-node assignments.
When a per-core assignment is correct, the gain is small (and is getting
smaller in manycores); when it is wrong, the penalty is huge
(and increasing with scale).
They are therefore made conservatively, and favor long regions that
allow for accurate measurements while covering enough of the program
execution time to be worth it.
That, however, reduces opportunity and increases the penalty for being
wrong.
Conversely, the benefits obtained from a correct global assignment are very 
similar to the penalty for being wrong, regardless of scale.
Scaling can be applied more aggressively and on shorter regions, further
reducing the cost of being wrong and enabling recovery, after learning
through measuring, of bad decisions.
