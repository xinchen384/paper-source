\section{Background and Motivation}
\label{sec:motiv}


{\it Slack} is the most often used measure to identify DVFS
opportunities in scientific codes and it is commonly defined as time
spent blocked or waiting in communication calls: waiting tasks can be
slowed down. As MPI two-sided has been dominant, most of the existing
approaches~\cite{adagio,kappiah2005sc,mpicluster,etempl,hybrid} are
tailored for its semantics.

Existing MPI energy optimizations try to construct a critical path
through the program execution and minimize slack: ranks on the critical
path (slowest ranks) need to run fast, while all others can be (down) scaled
to ``arrive just in time."
At the heart of these approaches is the ability to
predict the running time on each rank between two synchronization
operations, either point to point ({\tt MPI\_Send/MPI\_Recv}) or
collective (e.g.\ {\tt MPI\_Barrier}).  Initial studies~\cite{roundtree2007sc}
used offline, trace based analyses that often solve a global
optimization problem.  For generality and scalability, modern
approaches use online analyses. Rountree et al~\cite{adagio} present
Adagio, which uses context sensitive techniques to predict the critical
path at runtime and minimize slack for each MPI communication
%%WLAV: Note: Adagio instruments ALL MPI calls, except for Alltoall,
%%      because of the duration in their FT benchmark (they only want
%%      to slow down reaching the call, not the time in the call; see
%%      their paper).
%%      There is a selection file allowing you to remove MPI methods
%%      from the list. They do remove a bunch of them per the file
%%      in their distribution, but not, surprisingly Irecv & friends.
call in its context.

We do embrace the notion that demonstrating successful online analyses
is mandatory for the future adoption of DVFS techniques, either in
hardware or software. Our conjecture when starting this work was that
emerging programming models and optimization techniques together with
modern hardware architecture all work against techniques using
quantitative prediction of execution times at the granularity required
for successful DVFS.

Our survey of the techniques proposed
in~\cite{adagio,mpicluster,hybrid,kappiah2005sc,roundtree2007sc} shows
that they were evaluated only on codes with  {\it static domain decomposition} and static load
balancing:  each rank in
each iteration works on the same data partition, making execution time
{\it predictable}. In newer codes that use {\it dynamic load
balancing}, each rank in each iteration can process different data
partitions, leading to more {\it unpredictable} duration per rank per
iteration. To our knowledge, DVFS optimizations for dynamically load
balanced codes at scale have not yet been demonstrated.

All
codes\footnote{Bar one trivial usage of {\tt MPI\_Isend/MPI\_Irecv} in
  one code.} surveyed in literature use blocking two-sided communication.
Non-blocking communication and overlap add another dimension of noise
and unpredictable~\cite{IancuS07} behavior. To our knowledge, energy
optimizations on applications using one-sided communication have not
yet been demonstrated.
On these codes, the existing state-of-the-art of scaling fails: dynamic
load balancing explicitly attempts to remove slack and makes any slack
that is left-over unpredictable.
Furthermore, one-sided communication removes a great many (implicit)
synchronization points.

 Finally, modern CPU hardware is
tuned for energy efficiency and employs aggressive dynamic control. It
begs the question how much software techniques (perhaps global) can
still improve
on existing hardware mechanisms in practice.
From the hardware perspective many
approaches~\cite{adagio,mpicluster,Ribic,hybrid,kappiah2005sc,roundtree2007sc}
have been validated assuming availability of per core DVFS, some even using a single core in configurations with
as many as eight cores per socket.  Modern hardware either offers
only per socket control (e.g.\ Intel Ivy Bridge), or per-core control
interferes with the execution on cores sharing resources within the
socket (e.g.\ hyperthreads, rings on Intel
Haswell\cite{hackenberg2015}, caches). Other system components
(e.g.\ the Mellanox FDR InfiniBand\footnote{During this work we have
  uncovered performance and functionality bugs in the current
  generation of Mellanox drivers.} driver)  may require a single
frequency assignment per socket or node.
Another hardware trend is the presence of "Turbo-boost", which allows
individual cores to run at a higher frequency depending on system load.
Not only does this reduce the amount of slack in a natural way, it also
makes schedules harder to control, as the initial drop from Turbo-boosted
frequencies to the first user-selectable frequency tends to be huge (of
the order of 20\%-35\%, depending on load). An algorithm that relies on a
detailed per-core schedule therefore needs to find ``imbalance'' of
similar  magnitude 
before it can apply DVFS.
  For practicality, extensions to existing techniques to
provide coarser grained control over groups of cores are
required. And these are even more desirable for whole system
optimization goals.


\subsection{Predicting Timings for  Dynamic Behavior}

We first examine what is predictable in an application that uses
dynamic load balancing.
NWChem~\cite{valiev} is a framework for large scale computational chemistry codes,
 written to use as a
 communication transport either MPI two-sided, or one-sided
 communication with MPI 3.0, ARMCI~\cite{armci}, ComEx~\cite{comex} or
 GASNet~\cite{gasnet}; see Section~\ref{sec:nwchem} for more details.



Given that one-sided communication ({\tt Put} or {\tt Get}) does not
carry any  inter-task synchronization semantics like {\tt
  MPI\_Send/MPI\_Recv} pairs do, we have started by trying
to predict slack between two barrier operations. 
We configure NWChem to
use one-sided communication and  collect the durations per task
between  barriers,  taken as the difference between the exit time of one
barrier and the entrance time of the next. The slack
associated with a task is the difference in  execution time when compared
with the slowest task.


\begin{figure}[t]
% \centering 
 \includegraphics[width=3.5in]{predictivity.pdf}
\caption{\footnotesize\textit{Quality of prediction of the critical path based on
calling context and process in NWChem, for a run of 1024 processes.
Shown is the ranking of the critical (top) and fastest (bottom) process, in the
subsequent re-occurrences of tasks.
The probability of the critical path remaining critical, and of the fastest
remaining so, is less than 10\%.}}
\label{fig:predictivity}
\end{figure}


\begin{figure*}[t!]
\vspace{-0.8in}
\begin{minipage}[t]{\textwidth}
\begin{minipage}[c][1\width]{0.5\textwidth}
\centering%
\includegraphics[width=\textwidth]{stdev_memcpu}
\end{minipage}
\begin{minipage}[c][1\width]{0.5\textwidth}
\centering%
\includegraphics[width=\textwidth]{skew_memcpu}
\end{minipage}
\vspace{-0.7in}
\caption{\label{fig:cpumemnoise} \textit{\footnotesize Variability measures
for a mixed parallel computations on Edison: dispersion (left) and skewness (right) of
the timing distributions across ranks, as a function of scale and fraction of
CPU-limited code.
At 100\%, the code is fully CPU intensive, mixing in memory- and network-limited code
(at roughly a 3:1 ratio) until no longer CPU-limited.}}
\end{minipage}
\end{figure*}

For each region (i.e.\ barrier pair), we label each task with its index
in the sorted array of all timings for all tasks. We collect
these rankings per process and calling context (described in
Section~\ref{sec:design}).
We select only those regions that are long enough (at least
$300{\mu}s$\footnote{About $3 \times$ the latency of the  system specific DVFS control.}) and
have a minimum of $5\%$ difference in duration between the fastest and slowest
process, compared to their average (i.e.\ there is a minimum $5\%$ slack).
This selects regions for which DVFS is potentially practical and beneficial.
%WLAV: note here that for communication based scheduling, you'd want to
% select the shorter periods, assuming you can cluster them (which you can).
% Conversely, communication scheduling does not work for longer periods, as
% the communication time is fixed, hence relatively shorter.

We select those calling contexts that occur at least $10$ times,
providing enough repetition to  allow  both predicting/learning and 
scaling to occur.
From these, we take the slowest process, i.e.\ the critical path, at the first
occurrence of each, and plot their ``ranking", expressed as a
percentile from fastest to slowest, at each subsequent re-occurrence of the
same context. 
The results are in the top of Figure~\ref{fig:predictivity}, for a run with
$1,024$ processes.
If the time duration of the first occurrence of a calling context can be used
to predict the durations on subsequent calls, then there should be a sharp
peak at $100\%$, i.e.\ the critical path should remain close to
critical.
What we observe, however, is an almost flat distribution, that moderately
tapers off towards zero, with less than $10\%$ of the critical path
``predictions'' being on the mark.

Mispredicting the critical path leads only to missed opportunities.
Mispredicting the fastest path, i.e.\ the process to  scale
down the most, can have  performance consequences.
A similar analysis for the fastest path
(Figure~\ref{fig:predictivity}), shows that it is as hard to predict, with
the same $\sim 10\%$ hit rate. 

Overall, this data indicates  that schemes relying on predicting the per rank
duration of execution~\cite{adagio,mpicluster,hybrid,kappiah2005sc}
are likely to under-perform for our target application,
as an incorrect schedule is likely on 90\% of the tasks/cores affected. 
