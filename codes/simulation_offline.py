import cPickle, os, sys
from dataobjects import *

BASEPATH = sys.argv[1]
if not os.path.exists(BASEPATH):
   print 'base dir', BASEPATH, 'does not exist'
   sys.exit(1)

BASEPATH = os.path.abspath(BASEPATH)

print 'using base dir:', BASEPATH

sys.path.append(BASEPATH)
from common import *

# parameters
PENALTY           = 100       # switching cost in us (included in timing information)
STDEV_THRESHOLD   = 0.01      # relative stdev threshold, should be 0.01 - 0.20
SKEW_CUTOFF       = 3.0       # maximum allowed skewness
BRIDGE_THRESHOLD  = 2000      # allowed (possible) CPU-intensive region gap
SHORT_REGION      = 200       # period when to collect anyway, as communication dominates
                              #   barrier time is 40-90us
TIME_THRESHOLD    = 1000      # cut-off to accept a cluster for scaling

if len(sys.argv) > 2:
   BRIDGE_THRESHOLD = int(sys.argv[3])
if len(sys.argv) > 3:
   SHORT_REGION      = int(sys.argv[4])
print 'Using cut-offs (BRIDGE_THRESHOLD, SHORT_REGION):', BRIDGE_THRESHOLD, SHORT_REGION

CLUSTER_CUTOFF = 0
if len(sys.argv) > 4:
   CLUSTER_CUTOFF = TIME_THRESHOLD
   print 'CLUSTERING DISABLED!'

input = open(os.path.join(BASEPATH, 'stdevs.pkl'), 'rb')
data = cPickle.load(input)

# selected frequencies for comparison
ONLINE = FREQUENCIES[FREQUENCIES.index('default')]
ONLINE_DATA = data[ONLINE]

NREGIONS = len(ONLINE_DATA.stdevs)

class result(object):
   def __init__(self):
      self.energy = 0.
      self.time   = 0.

default = result()
lowered = result()

decisions = dict()
contextAvgs = dict()

class CountStats:
   def __init__(self):
      self.count = 0
      self.timed = 0.

class Statistics(object):
   def __init__(self):
      self.switches  = 0
      self._time     = 0.
      self._energy   = 0.
      self._freqdist = dict()

      for freq in FREQUENCIES:
         self._freqdist[freq] = CountStats()

   def switch(self):
      self.switches += 1

   def switching_cost(self):
      return self.switches * PENALTY

   def time(self, freq, iregion):
      tdata = data[freq]
      self._time   += tdata.regions[iregion]
      self._freqdist[freq].count += 1
      self._freqdist[freq].timed += tdata.regions[iregion]

   def time_spent(self):
      return self._time

   def energy(self, freq, iregion):
      tdata = data[freq]
      self._energy += tdata.regions[iregion]*(tdata.power[iregion]+tdata.power[iregion])/2.
      self._freqdist[freq].count += 1
      self._freqdist[freq].timed += tdata.regions[iregion]

   def energy_spent(self):
      return self._energy

   def report(self):
      print 'distribution (count, time):'
      avg_freq, lowered_time = 0., 0.
      for freq in FREQUENCIES:
         if freq == 'online':
            continue
         stats = self._freqdist[freq]
         print freq, '% 8d  / % 8.1f' % (stats.count, stats.timed/1E6)

         if freq == 'default':
            continue

         avg_freq     += int(freq)*stats.timed
         lowered_time += stats.timed
      print 'best frequency: %3.1fGHz (rounded from: %4.2f)' % tuple([avg_freq/lowered_time/1E6]*2)

stats = Statistics()

runningDecisions = list()

class DecisionState:
   CLUSTERING = 'C'
   SCALING    = 'S'
   BRIDGING   = 'T'

   def __init__(self):
      self.reset(0)

   def reset(self, context):
      self.state      = self.CLUSTERING
      self._decisions = dict()
      self._dtime     = 0.
      self._bridge    = dict()
      try:
         del decisions[context]
      except KeyError:
         pass

   def collect(self, context, dtime):             # accept a noisy region
      if self.state == DecisionState.BRIDGING:
         self._dtime = 0  # start of a potential new cluster
         self.state = DecisionState.CLUSTERING
      if self.state   == DecisionState.CLUSTERING:
         return self.cluster(context, dtime)
      elif self.state == DecisionState.SCALING:
         return self.scale(context, dtime)
      assert not "Unknown state!"

   def reject(self, context, dtime):              # accept a non-noisy region
      if SHORT_REGION < dtime:
       # long(ish) region, still if scaling already allow possible extension
         if self.state == DecisionState.SCALING:
            self.state = DecisionState.BRIDGING
         if self.state == DecisionState.BRIDGING:
            return self.bridge(context, dtime)
         return self.reset(context)
      else:
       # short region, so communication dominates: collect anyway
         return self.collect(context, dtime)

   def cluster(self, context, dtime):
      self._dtime += dtime
      self._decisions[context] = 1

      if TIME_THRESHOLD < self._dtime:
         self.state = DecisionState.SCALING
         return self.scale(context, dtime)

   def scale(self, context, dtime):
      if self._decisions:
         self._dtime = 0
         decisions.update(self._bridge)
         decisions.update(self._decisions)
         self._decisions = dict()
         self._bridge    = dict()
      decisions[context] = 1

   def bridge(self, context, dtime):
      self._dtime += dtime
      self._bridge[context] = 1
      if BRIDGE_THRESHOLD < self._dtime:
         return self.reset(context)

dstate = DecisionState()

#
## replay decision simulation
#

print 'Now replaying decisions ... '
for iregion in xrange(NREGIONS):
   if ONLINE_DATA.stdevs[iregion] > STDEV_THRESHOLD and ONLINE_DATA.skews[iregion] < SKEW_CUTOFF and ONLINE_DATA.regions[iregion] > CLUSTER_CUTOFF:
      dstate.collect(ONLINE_DATA.contexts[iregion], ONLINE_DATA.averages[iregion])
   else:
      dstate.reject( ONLINE_DATA.contexts[iregion], ONLINE_DATA.averages[iregion])

   try:
      schedule = decisions[ONLINE_DATA.contexts[iregion]]
      assert schedule == 1
      runningDecisions.append( schedule )
   except KeyError:
      runningDecisions.append(0)


default.energy = ONLINE_DATA.power[0] * ONLINE_DATA.regions[0]
for iregion in xrange(1, NREGIONS):
   default.energy += ONLINE_DATA.regions[iregion]*(ONLINE_DATA.power[iregion]+ONLINE_DATA.power[iregion])/2.
default.time   = sum(ONLINE_DATA.regions)


#
## simulate energy assignment
#

class VoltageState(object):
   HIGH = 1
   LOW  = 0

   def __init__(self):
      self.state   = VoltageState.HIGH
      self.outstanding = list()

      stats.energy( 'default', 0 )
      stats.time(   'default', 0 )

   def down(self, iregion):
      self.outstanding.append(iregion)
      if self.state == VoltageState.HIGH:
         self.state = VoltageState.LOW
         stats.switch()
         self.commit()

   def commit(self):
    # select best frequency over the past cluster, then commit
      best_energy, best_time = (1.E64, 1.E64)
      best_freq   = None
      for freq in FREQUENCIES:
         if freq in ['default', 'online']:
            continue

         tdata = data[freq]

         ref_time = 0.
         for iregion in self.outstanding:
            ref_time += ONLINE_DATA.regions[iregion]

         time, energy = 0., 0.
         for iregion in self.outstanding:
            time   += tdata.regions[iregion]
            energy += tdata.regions[iregion]*(tdata.power[iregion]+tdata.power[iregion])/2.

       # only select if best energy is achieved with at most 5% loss of time, and is
       # not out of whack compared to the reference time
         if energy < best_energy and time < 1.05*best_time and ref_time < time:
            best_energy, best_freq, best_time = energy, freq, time

      if not best_freq:
       # this is the case when timings are way faster in the lower frequencies than in
       # default: average the times and simply pick 2.1GHz (this is a bias, but ignoring
       # these regions shows that 2.1GHz is overall preferred)
         best_time = 0.
         for freq in FREQUENCIES:
            if freq in ['online']:
               continue

            tdata = data[freq]
            for iregion in self.outstanding:
               best_time += tdata.regions[iregion]
         best_time /= len(FREQUENCIES) - 1

         best_freq = '2100000'
         avg_pow, bf_time = 0., 0.
         tdata = data[best_freq]
         for iregion in self.outstanding:
            avg_pow += tdata.regions[iregion]*tdata.power[iregion]
            bf_time += tdata.regions[iregion]
         avg_pow /= bf_time
         stats._time   += best_time
         stats._energy += avg_pow * best_time
         stats._freqdist[freq].count += len(self.outstanding)
         stats._freqdist[freq].timed += best_time

      else:

         for iregion in self.outstanding:
            stats.time(  best_freq, iregion)
            stats.energy(best_freq, iregion)

      self.outstanding = list()

   def up(self, iregion):
      if self.state == VoltageState.LOW:
         self.state = VoltageState.HIGH
         stats.switch()
      stats.time(  'default', iregion)
      stats.energy('default', iregion)

   def isUp(self):
      return self.state == VoltageState.HIGH


print 'Simulating online assignment ...'
vstate = VoltageState()
frac_lower_time = 0.
for iregion in xrange(1, NREGIONS):
   if runningDecisions[iregion]:
      vstate.down(iregion)
      frac_lower_time += ONLINE_DATA.regions[iregion]
   else:
      vstate.up(iregion)
vstate.commit()

# incorporate costs:
extra_energy = stats.energy_spent() / stats.time_spent() * stats.switching_cost()
extra_time   = stats.switching_cost()

fraction = 100.*frac_lower_time/default.time
savings  = -((stats.energy_spent() + extra_energy ) / default.energy - 1. )*100.
cost     = -((stats.time_spent()   + extra_time   ) / default.time   - 1. )*100.

print '   totals  :', stats.time_spent()/1.E6, default.time/1.E6
print '   fraction:', fraction
print '   savings :', savings
print '   cost    :', cost

print
stats.report()

print
print 'cross-check: # barriers:', len(ONLINE_DATA.regions)
print '             # switches:', stats.switches

print ' --- '
print '% 5.1f%%  /% 6.1f%%   /% 9d     /% 9.1f%%' % (savings, -cost, stats.switches, fraction)
