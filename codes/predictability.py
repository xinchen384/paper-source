import cPickle, os, sys
from dataobjects import *

import ROOT

f = ROOT.TFile("balance.root", "recreate")
h1 = ROOT.TH1F("critical", "critical path", 100, 0., 100.)
h2 = ROOT.TH1F("fastest", "fastest path",   100, 0., 100.)


BASEPATH = sys.argv[1]
if not os.path.exists(BASEPATH):
   print 'base dir', BASEPATH, 'does not exist'
   sys.exit(1)

BASEPATH = os.path.abspath(BASEPATH)

print 'using base dir:', BASEPATH

input = open(os.path.join(BASEPATH, 'balance.pkl'), 'rb')
data = cPickle.load(input)

rank_per_context = dict()

NREGIONS = len(data)
NRANKS   = len(data[0].barriers)

NSLOWEST = 1

fastest_path  = dict()
critical_path = dict()

perc_done = 5
for iregion in xrange(NREGIONS):
   perc = int(iregion/float(NREGIONS)*100)
   if perc > perc_done:
      print 'processing:', perc_done, '%'
      perc_done += 5
   ri = data[iregion]

   barriers = ri.barriers
   barriers_copy = barriers[:]
   barriers.sort()

   bar_avg = sum(barriers)/float(NRANKS)
   if bar_avg < 300:
      continue

   if ((barriers[-1]-barriers[0])/bar_avg) < 0.05:
      continue

   for islow in range(-NSLOWEST, 0):
      if ri.context in critical_path:
       # for what was first the slowest process for this context, get
       # it's current standing
         val_critical = barriers_copy[ critical_path[ri.context][0] ]
         standing_val_critical = barriers.index(val_critical)
         percentile = 100.*standing_val_critical/float(NRANKS)
         critical_path[ri.context].append(percentile)
      else:
       # get the process number that was the slowest
         if barriers_copy.count(barriers[islow]) == 1:
            slowest_val_rank = barriers_copy.index(barriers[islow])
         else:
            search_val = barriers[islow]
            for i in xrange(len(barriers_copy)):
               if barriers_copy[i] == search_val:
                  ll = list()
                  try:
                     while barriers_copy[i] == search_val:
                        ll.append(i)
                        i += 1
                  except IndexError:
                     pass
                  slowest_val_rank = int(round(sum(ll)/len(ll)))
                  break
         critical_path.setdefault(ri.context, list()).append(slowest_val_rank)

   if ri.context in fastest_path:
    # for what was first the fastest process for this context, get
    # it's current standing
      val_fastest = barriers_copy[ fastest_path[ri.context][0] ]
      standing_val_fastest = barriers.index(val_fastest)
      percentile = 100.*standing_val_fastest/float(NRANKS)
      fastest_path[ri.context].append(percentile)
   else:
     # get the process number that was the fastest
      if barriers_copy.count(barriers[0]) == 1:
         fastest_val_rank = barriers_copy.index(barriers[0])
      else:
         search_val = barriers[0]
         for i in xrange(len(barriers_copy)):
            if barriers_copy[i] == search_val:
               ll = list()
               try:
                  while barriers_copy[i] == search_val:
                     ll.append(i)
                     i += 1
               except IndexError:
                  pass
               fastest_val_rank = int(round(sum(ll)/len(ll)))
               break
      fastest_path.setdefault(ri.context, list()).append(fastest_val_rank)

total = 0
for context, indices in critical_path.items():
   total += len(indices)
print NREGIONS, total

def do_fill(d, h):
   for context, indices in d.items():
      if len(indices) >= 10:

         for val in indices[1:]:
            h.Fill(val)

do_fill(critical_path, h1)
do_fill(fastest_path, h2)

f.Write()
f.Close()
