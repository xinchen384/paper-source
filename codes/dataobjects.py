FREQUENCIES = [
  'default', 'online',
  '2400000', '2200000', '2000000', '1800000', '1600000']

class node_info(object):
   def __init__(self):
      self.cores    = dict()
      self.contexts = list()
      self.power    = list()
      self.papi     = list()

class job_info(object):
   def __init__(self):
      self.barriers = list()
      self.regions  = list()
      self.decision = list()       # online only

class run_info(object):
   def __init__(self):
      self.skews     = list()
      self.stdevs    = list()
      self.averages  = list()

      self.contexts  = list()
      self.regions   = list()
      self.power     = list()

class region_info(object):
   def __init__(self):
      self.context   = None
      self.barriers  = list()
