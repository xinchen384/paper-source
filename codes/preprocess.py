import glob, math, os, sys
from dataobjects import *

BASEPATH = sys.argv[1]
if not os.path.exists(BASEPATH):
   print 'base dir', BASEPATH, 'does not exist'
   sys.exit(1)

BASEPATH = os.path.abspath(BASEPATH)

print 'using base dir:', BASEPATH

sys.path.append(BASEPATH)
from common import *

# Format of logs in a run:
#
# context:barriertime:regiontime:power:<online decision>
#
# context = hash of stack trace, uniquely defines barrier start
# barriertime = for pair (B1, B2), time from end of B1 to start of B2
# regiontime = for pair (B1, B2), time from end of B1 to end of B2
#  (by definition: regiontime > barriertime)
# power = current power in Watt
# <online decision> = 0 or 1 (== scaling no/yes)

def enter_run1():
   for freq in FREQUENCIES:
      try:
         os.chdir( os.path.join( BASEPATH, freq, 'run1' ) )
      except OSError:
         pass
      else:
         break

def collect_nids():
   nids = []
   enter_run1()
   for lfile in glob.glob( "*.lock" ):
      nids.append(lfile[3:-5])
   return nids
NIDS = collect_nids()

def collect_coreids():
   coreids = []
   enter_run1()
   for lfile in glob.glob('LOG_nid'+NIDS[0]+'_*_*.log.gz'):
      coreids.append(lfile[lfile.rfind('_')+1:-7])
   coreids.sort()
   return coreids
COREIDS = collect_coreids()

# Data collection:
#
# There are 4-5 runs for each frequency, average their numbers (we average
# stdevs rather than variances: we don't want an estimate of the overal
# variance, but of the average stdev; same for skews).
# Runs can be rejected with the SKIPRUNS data structure.

data = {}

for freq in FREQUENCIES:
   try:
      os.chdir( os.path.join( BASEPATH, freq ) )
   except OSError:
      continue

   FREQUENCY = dict()
   os.chdir( os.path.join( BASEPATH, freq ) )

   RUNS = glob.glob('run?')
   RUNS.sort()
   for r in RUNS[:]:
      if r in SKIPRUNS[freq]:
         RUNS.remove( r )
   NRUNS = len(RUNS)

   if not NRUNS:
      continue

   print 'processing', freq
   for runid in RUNS:
      os.chdir( os.path.join( BASEPATH, freq, runid ) )

      barrier_times = list()

      contexts      = list()
      regions       = list()
      power         = list()

      for inid in range(len(NIDS)):

         nid = NIDS[inid]
         print '... processing runid:', runid, 'nid:', nid, '(%d)' % (inid+1)

         for coreid in COREIDS:

            print '   ... processing coreid:', coreid

            logfiles = glob.glob('LOG_nid'+nid+'*_'+coreid+'.log.gz')
            assert len(logfiles) == 1

            barriers = list()
            for l in os.popen('gunzip -c ' + logfiles[0]).readlines():
               if l[0] == '#':
                  break             # done with regions data
               l = l.split(':')
               barriers.append(float(l[1]))

               pwr = float(l[3])
               if pwr and nid == NIDS[0]:
                  contexts.append( int(l[0]) )
                  power.append( pwr )
                  regions.append( float(l[2]) )

            barrier_times.append(barriers)

    # stdev over all cores
      NRANKS   = len(barrier_times)
      assert NRANKS == len(COREIDS)*len(NIDS)
      NREGIONS = len(barrier_times[0])

      skews     = list()
      relstdevs = list()
      averages  = list()
      for iregion in xrange(NREGIONS):

         avg = 0.
         for barriers in barrier_times:
            avg += barriers[iregion]
         avg /= NRANKS

         averages.append(avg)

         stdev = 0.
         for barriers in barrier_times:
            diff = barriers[iregion] - avg
            stdev += diff*diff
         stdev = math.sqrt(stdev/NRANKS)

         relstdevs.append(stdev/avg)

         skw = 0.
         if stdev != 0:
            for barriers in barrier_times:
               diff = (barriers[iregion] - avg)/stdev
               skw += diff**3
         skw /= NRANKS

         skews.append(skw)

      ri = run_info()
      ri.skews     = skews
      ri.stdevs    = relstdevs
      ri.averages  = averages

      ri.contexts  = contexts
      ri.regions   = regions
      ri.power     = power

      FREQUENCY[runid] = ri

   print 'now averaging across runs ...'
   ri = run_info()
   assert len(RUNS) == NRUNS
   for iregion in xrange(NREGIONS):
      skw, std, avg, reg, pwr = (0., 0., 0., 0., 0.)
      for runid in RUNS:
         skw += FREQUENCY[runid].skews[iregion] 
         std += FREQUENCY[runid].stdevs[iregion] 
         avg += FREQUENCY[runid].averages[iregion]

         reg += FREQUENCY[runid].regions[iregion] 
         pwr += FREQUENCY[runid].power[iregion] 

      ri.skews.append(   skw/NRUNS)
      ri.stdevs.append(  std/NRUNS)
      ri.averages.append(avg/NRUNS)

      ri.regions.append( reg/NRUNS)
      ri.power.append(   pwr/NRUNS)

   ri.contexts = FREQUENCY[RUNS[0]].contexts
   data[freq] = ri

import cPickle
os.chdir(BASEPATH)
output = open("stdevs.pkl", "wb")
cPickle.dump(data, output)
