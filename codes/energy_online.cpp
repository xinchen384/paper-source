// * BeginRiceCopyright *****************************************************
//
// Copyright ((c)) 2002-2014, Rice University
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// * Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//
// * Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the distribution.
//
// * Neither the name of Rice University (RICE) nor the names of its
//   contributors may be used to endorse or promote products derived from
//   this software without specific prior written permission.
//
// This software is provided by RICE and contributors "as is" and any
// express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular
// purpose are disclaimed. In no event shall RICE or contributors be
// liable for any direct, indirect, incidental, special, exemplary, or
// consequential damages (including, but not limited to, procurement of
// substitute goods or services; loss of use, data, or profits; or
// business interruption) however caused and on any theory of liability,
// whether in contract, strict liability, or tort (including negligence
// or otherwise) arising in any way out of the use of this software, even
// if advised of the possibility of such damage.
//
// ******************************************************* EndRiceCopyright *

#include <stdio.h>
#include <execinfo.h>
#include <stdlib.h>
#include <vector>
#include <stdint.h>
#include <sys/stat.h>
#include <iostream>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <sstream>
#include <utility>
#include <limits.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sched.h>
#include <sstream>
#include <map>
#include <mpi.h>

#include <google/dense_hash_map>

#include "pmon.h"


// marker for in log file messages
#define ENERGY "ENERGY: "

using namespace std;
using google::dense_hash_map;


// switching cost in us, based on Teller or TURBO Diaries paper
// on Edison: prefer to run the penalty in offline simulation
#define PENALTY                  0
const double STDEV_THRESHOLD   = 0.01; // relative stdev threshold, should be 0.01 - 0.20
const double SKEW_CUTOFF       = 5.0;  // maximum allowed skewness
const int    CONNECT_THRESHOLD = 2000; // allowed (possible) CPU-intensive region gap
const int    SHORT_REGION      = 400;  // period when to collect anyway, as communication dominates
                                       //   barrier time is 40-90us
const int    TIME_THRESHOLD    = 1000; // cut-off to accept a cluster for scaling

const int DVFS_UP   = 0;
const int DVFS_DOWN = 1;

typedef dense_hash_map<uint64_t, int> Decisions_t;
Decisions_t gDecisions;

static const char CLUSTERING = 'C';
static const char SCALING    = 'S';
static const char CONNECTING = 'T';

class DecisionState {
public:
    DecisionState() {
        gDecisions.set_empty_key((uint64_t)-1);
        gDecisions.set_deleted_key((uint64_t)-2);

        m_decisions.set_empty_key((uint64_t)-1);
        m_decisions.set_deleted_key((uint64_t)-2);

        m_bridge.set_empty_key((uint64_t)-1);
        m_bridge.set_deleted_key((uint64_t)-2);

        reset(0);
    }

    void reset(uint64_t context) {
        m_state     = CLUSTERING;
        m_time      = 0.;
        m_decisions.clear();
        m_bridge .clear();

        gDecisions.erase(context);
    }

    void collect(uint64_t context, float dtime) {     // accept a noisy region
        if (m_state == CONNECTING) {
            m_time = 0.;           // start of a potential new cluster
            m_state = CLUSTERING;
        }

        if (m_state == CLUSTERING) {
            return cluster(context, dtime);
        } else if (m_state == SCALING) {
            return scale(context, dtime);
        } else {
            assert(!"received unknown state!");
        }
    }

    void reject(uint64_t context, float dtime) {      // reject a non-noisy region
        if (SHORT_REGION < dtime) {
        // long(ish) region, still if scaling already allow possible extension
            if (m_state == SCALING) m_state = CONNECTING;
            if (m_state == CONNECTING)
                return connect(context, dtime);
            return reset(context); 
        }
        // else: short region, so communication dominates: collect anyway
        return collect(context, dtime);
    }

    void cluster(uint64_t context, float dtime) {
        m_time += dtime;
        m_decisions[context] = 1;

        if (TIME_THRESHOLD < m_time) {
            m_state = SCALING;
            return scale(context, dtime);
        }
    }

    void scale(uint64_t context, float dtime) {
        if (!m_decisions.empty() || !m_bridge.empty()) {
            m_time = 0.;
            for (Decisions_t::iterator it = m_bridge.begin();
                 it != m_bridge.end(); ++it) {
                gDecisions[it->first] = it->second;
            }
            m_bridge.clear();

            for (Decisions_t::iterator it = m_decisions.begin();
                 it != m_decisions.end(); ++it) {
                gDecisions[it->first] = it->second;
            }
            m_decisions.clear();
        }
        gDecisions[context] = 1;
    }

    void connect(uint64_t context, float dtime) {
        m_time += dtime;
        m_bridge[context] = 1;
        if (CONNECT_THRESHOLD < m_time)
            return reset(context);
    }

    bool doScale(uint64_t context) {
        Decisions_t::iterator it = gDecisions.find(context);
        return it != gDecisions.end() ? it->second : false;
    }

private:
    Decisions_t m_decisions;
    Decisions_t m_bridge;
    double m_time;
    char m_state;
} gDecisionState;


extern "C" {

#define BARRIER_FN_NAME "MPI_Barrier"

#define REAL_FUNCTION(name)  __real_ ## name
#define WRAPPED_FUNCTION(name)  __wrap_ ## name
#define TIME_SPENT(start, end) (end.tv_sec * 1000000 + end.tv_usec - start.tv_sec*1000000 - start.tv_usec)

    static bool ONLINE_ANALYSIS = false;
    static bool COLLECT_TRACES  = false;

    static float SAFETY_CUTOFF = 0.f;
    
    static FILE* output;
    int myRank, amPowerReader;
    struct timeval startTime, endTime;
    static struct timeval barr_t1, barr_t2;
    static struct timeval last_sample = { 0, 0 };;

    // a region is from the end of barrier 1 (B1) to the end of barrier 2 (B2)
    // for a consecutive pair (B1, B2) and collected in regionTimes; barrier
    // times end a the start of B2 instead, and can be used to calculate stdev
    static vector<uint64_t> barrierTimes, regionTimes;
    static vector<double>   barrierPower;
    static double curpower = 0.;

    typedef std::map<uint64_t, std::vector<uint64_t> > TraceMap_t;
    static TraceMap_t backTraces;
    static std::vector<uint64_t> contexts;

    static std::vector<int> runningDecisions;
    static uint64_t lastContext;
    static int currentDVFSState = DVFS_UP;

    static dense_hash_map<uint64_t, float> contextAverages;

    static void WriteExperimentData() {
        if ( ONLINE_ANALYSIS ) {
            for ( int i = 0; i < barrierTimes.size(); i++ ) {
                fprintf( output, "%lu:%lu:%lu:%0.f:%d\n",
                   contexts[i], barrierTimes[i], regionTimes[i], barrierPower[i], runningDecisions[i] );
            }
        } else {
            for ( int i = 0; i < barrierTimes.size(); i++ ) {
                fprintf( output, "%lu:%lu:%lu:%0.f\n",
                   contexts[i], barrierTimes[i], regionTimes[i], barrierPower[i] );
            }
        }

        if (COLLECT_TRACES) {
            fprintf( output, "# contexts and traces:\n" );
            for ( TraceMap_t::iterator i = backTraces.begin(); i != backTraces.end(); ++i ) {
                const std::vector<uint64_t>& trace = i->second;
                fprintf( output, "%lu", i->first );
                for ( int j = 0; j < trace.size(); j++ )
                    fprintf( output, ":%lu", trace[j] );
                fprintf( output, "\n" );
            }
        }
    }


    static void CloseLogFile() {
        fclose( output );
    }

    static void CreateLogFile(int rank) {
        assert( output == NULL );
        std::stringstream ss;
        ss << "gzip -f >> LOG_";
        char hostname[HOST_NAME_MAX];
        if ( gethostname(hostname, HOST_NAME_MAX) == 0 )
            ss << hostname << "_";
        pid_t pid = getpid();
        int cpuid = sched_getcpu();
        ss << pid << "_" << cpuid << ".log.gz";
        //output = fopen(ss.str().c_str(), "w");
        output = popen( ss.str().c_str(), "w" );

        std::stringstream lf;
        lf << hostname << ".lock";
        if ( open( lf.str().c_str(), O_CREAT | O_EXCL, S_IRUSR | S_IWUSR ) < 0 )
           amPowerReader = 0;
        else
           amPowerReader = 1;
    }

    static void CollectData() {
       if ( 50000 < TIME_SPENT(last_sample, barr_t2) || last_sample.tv_usec == 0 ) {
           last_sample = barr_t2;
           curpower  = amPowerReader ? PMON_sample_power_node()  : 0.;
       }
       barrierPower.push_back(curpower);
    }


#define ASM_LABEL(name)         \
asm volatile (".globl " #name );    \
asm volatile ( #name ":" )

    extern int REAL_FUNCTION(main)(int argc, char ** argv);
    extern void * main_fence1 __attribute__((weak));
    extern void * main_fence2 __attribute__((weak));
    static void * stackBottom = NULL;

    /*
     *  Returns: 1 if address is within the body of the function at the
     *  bottom of the application's call stack, else 0.
     */
    int HasStackEnded(void * insPtr, void * framePtr) {
        if (&main_fence1 <= insPtr && insPtr <= &main_fence2)
            return 1;
        if (framePtr > stackBottom)
            return 1;
        return 0;
    }

    int WRAPPED_FUNCTION(main)(int argc, char ** argv) {
        ASM_LABEL(main_fence1);
        stackBottom = alloca(8);
        strncpy((char *) stackBottom, "stakbot", 8);
        return REAL_FUNCTION(main)(argc, argv);
        ASM_LABEL(main_fence2);
    }

    static uint64_t StoreContextHash() {
        void ** curStackPointer = (void **) __builtin_frame_address(0);
        void * curRA = *(curStackPointer+1);
        uint64_t hash = 0;

        // Iterate over return addresses and sum them to get a hash
        while(!HasStackEnded(curRA, curStackPointer)) {
            hash += (uint64_t)curRA;
            hash += (hash << 10);
            hash ^= (hash >> 6);

            curStackPointer = (void **) (*curStackPointer);
            curRA = *(curStackPointer+1);
        }
        hash += (hash << 3);
        hash ^= (hash >> 11);
        hash += (hash << 15);

        if (COLLECT_TRACES) {
            curStackPointer = (void **) __builtin_frame_address(0);
            curRA = *(curStackPointer+1);

            std::vector<uint64_t> trace;
            trace.reserve(12);

            while(!HasStackEnded(curRA, curStackPointer)) {
                trace.push_back((uint64_t)curRA);
                curStackPointer = (void **) (*curStackPointer);
                curRA = *(curStackPointer+1);
            }

            std::vector<uint64_t>& ref = backTraces[hash];
            if (ref.empty()) ref.swap(trace);
        }
        contexts.push_back(hash);

        return hash;
    }

    static uint64_t GetContextHash() {
        void ** curStackPointer = (void **) __builtin_frame_address(0);
        void * curRA = *(curStackPointer+1);
        uint64_t hash = 0;

        // Iterate over return addresses and sum them to get a hash
        while(!HasStackEnded(curRA, curStackPointer)) {
            hash += (uint64_t)curRA;
            hash += (hash << 10);
            hash ^= (hash >> 6);

            curStackPointer = (void **) (*curStackPointer);
            curRA = *(curStackPointer+1);
        }
        hash += (hash << 3);
        hash ^= (hash >> 11);
        hash += (hash << 15);

        return hash;
    }


    extern int REAL_FUNCTION(MPI_Barrier)(MPI_Comm comm);
    extern int REAL_FUNCTION(MPI_Init)(int* argc, char** *argv);
    extern int REAL_FUNCTION(MPI_Finalize)(void);

    int WRAPPED_FUNCTION(MPI_Barrier)(MPI_Comm comm) {
        int retVal = MPI_SUCCESS;

        gettimeofday(&barr_t2, NULL);       // collected at entrance for stdev
        double dtime = TIME_SPENT(barr_t1, barr_t2);

        uint64_t currentContext = StoreContextHash();

        float tskew = 0., tstdev = 0., tavg = 0.;
        int commSize = 1;
        if (ONLINE_ANALYSIS) {
           if (gDecisionState.doScale(currentContext)) {
              if (currentDVFSState == DVFS_UP) {
                 if ( amPowerReader ) {
                 // twiddle frequency per node
#if(PENALTY)
                     usleep(PENALTY);
#endif
                     currentDVFSState = DVFS_DOWN;
                 }
                 // or twiddle frequency per socket/core

                 // can short-cut here?
              }
              runningDecisions.push_back(1);
           } else {
              if (currentDVFSState == DVFS_DOWN) {
                 if ( amPowerReader ) {
                 // twiddle frequency per node
#if(PENALTY)
                     usleep(PENALTY);
#endif
                     currentDVFSState = DVFS_UP;
                 }
                 // or twiddle frequency per socket/core
              }
              runningDecisions.push_back(0);
           }

           // calculate stdev (timed, as it is part of overhead)
           MPI_Comm_size(comm, &commSize);
           float* recvbuf = new float[commSize];

           float ftime = (float)dtime;
           retVal = MPI_Allgather(&ftime, 1, MPI_FLOAT, recvbuf, 1, MPI_FLOAT, comm);
           if (retVal != MPI_SUCCESS)
              return retVal;

           for (int isum = 0; isum < commSize; ++isum)
              tavg += recvbuf[isum];
           tavg /= commSize;

           double tvar = 0.;
           for (int isum = 0; isum < commSize; ++isum) {
              float diff = recvbuf[isum] - tavg;
              tvar += diff*diff;
           }
           tstdev = sqrt(tvar/commSize);

           if (tstdev != 0.) {
              for (int isum = 0; isum < commSize; ++isum) {
                  float diff = (recvbuf[isum] - tavg)/tstdev;
                  tskew += pow(diff, 3);
              }
           }
           tskew /= commSize;

           delete[] recvbuf;
        } else {
           retVal = REAL_FUNCTION(MPI_Barrier)(comm);
           if (retVal != MPI_SUCCESS)
              return retVal;
        }

        // original definition post-barrier timing to conclude the region
        barrierTimes.push_back(TIME_SPENT(barr_t1, barr_t2));
        gettimeofday(&barr_t2, NULL);       
        regionTimes.push_back(TIME_SPENT(barr_t1, barr_t2));

        // retrieve power data outside region timings
        CollectData();

        // re-sync to prevent non-PowerReader nodes regions inflation
        retVal = REAL_FUNCTION(MPI_Barrier)(comm);
        if (retVal != MPI_SUCCESS)
           return retVal;

        gettimeofday(&barr_t1, NULL);       // start next region

        if (ONLINE_ANALYSIS) {
           bool isSafe = true;
           dense_hash_map<uint64_t, float>::iterator it = contextAverages.find(lastContext);
           if (it != contextAverages.end()) {
           // the first entry is by definition unscaled, so can be used to catch problems
           // note that not being safe does not mean that we will not scale, as the region
           // may connect or be small enough to cluster
               if (runningDecisions.back())
                  isSafe = ( tavg / it->second ) < SAFETY_CUTOFF;
           } else {
               contextAverages[lastContext] = tavg;
           }

           // make a decision for next time (timed, as it is part of overhead)
           if ( isSafe && STDEV_THRESHOLD < tstdev/tavg && tskew < SKEW_CUTOFF ) {
               gDecisionState.collect(lastContext, tavg);
           } else {
               gDecisionState.reject(lastContext, tavg);
           }
        }

        lastContext = currentContext;

        return retVal;
    }


    int WRAPPED_FUNCTION(MPI_Init)(int* argc, char** *argv) {
        //std::cout << " Initializing (pid): " << (int)getpid() << std::endl;
        int retVal = REAL_FUNCTION(MPI_Init)(argc, argv);

        contextAverages.set_empty_key(-1);

        MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
        CreateLogFile(myRank);

        // in order to keep a single binary, drive this through envars
        if (getenv("WLAV_ONLINE_ANALYSIS")) {
            ONLINE_ANALYSIS = true;
            if (myRank == 0) printf(ENERGY"running online analysis\n");
        } else if (myRank == 0)
            printf(ENERGY"collecting standard measurements\n");

        // collecting traces is expensive, so only add if requested
        if (getenv("WLAV_COLLECT_TRACES")) {
            COLLECT_TRACES = true;
        }

        if (ONLINE_ANALYSIS) {
           // the online analysis requires a safety valve beyond which slowdown
           // should be flagged (e.g. for Teller that could be 3.8/3.4 = 1.12.
           const char* cutoff = getenv("WLAV_SAFETY_CUTOFF");
           if ( !cutoff ) {
               std::cerr << "envar WLAV_SAFETY_CUTOFF missing" << std::endl;
               exit(1);
           }

           SAFETY_CUTOFF = (float)atof(cutoff);
           if (myRank == 0) printf(ENERGY"using safety factor %f\n", SAFETY_CUTOFF);
        }

        // running decisions are pre the period, times post, so add a decision
        // here to get the right offset in the log (the last barrier is collected
        // in MPI_Finalize)
        runningDecisions.push_back(0);

        // same for contexts
        StoreContextHash();

        PMON_Init();

        if(myRank == 0) {
            gettimeofday(&startTime, NULL);
        }
        gettimeofday(&barr_t1, NULL);

        return retVal;
    }


    int WRAPPED_FUNCTION(MPI_Finalize)() {
        gettimeofday(&barr_t2, NULL);       // end previous region
        barrierTimes.push_back(TIME_SPENT(barr_t1, barr_t2));
        regionTimes.push_back(TIME_SPENT(barr_t1, barr_t2));

        CollectData();

        PMON_Finalize();

        int retVal = REAL_FUNCTION(MPI_Finalize)();

        if(myRank == 0) {
            gettimeofday(&endTime, NULL);
            printf("\n Execution Time = %lu \n", TIME_SPENT(startTime, endTime));
        }

        WriteExperimentData();
        CloseLogFile();

        return retVal;
    }
}
