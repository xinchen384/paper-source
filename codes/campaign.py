#!/usr/bin/python
import os, sys, commands

BASEPATH = sys.argv[1]
if not os.path.exists(BASEPATH):
   print 'base dir', BASEPATH, 'does not exist'
   sys.exit(1)

BASEPATH = os.path.abspath(BASEPATH)

print 'using base dir:', BASEPATH

basic_cmd = 'pypy simulation.py %s ' % BASEPATH
def run_cmd( cmd ):
   stat, out = commands.getstatusoutput( cmd )
   if stat:
      print out
      sys.exit(1)
   return out.split('\n')[-1]

for freq in [ '2.4', '2.2', '2.1', '2.0', '1.8', '1.6' ]:
   print ' to %s  ' % freq, run_cmd( basic_cmd + freq )
   print '- super  ',        run_cmd( basic_cmd + freq + ' 0' )
   print '- short  ',        run_cmd( basic_cmd + freq + ' 0 0' )
   print '- clust  ',        run_cmd( basic_cmd + freq + ' 0 0 1' )
   print
