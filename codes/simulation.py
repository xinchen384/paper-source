import cPickle, os, sys
from dataobjects import *

BASEPATH = sys.argv[1]
if not os.path.exists(BASEPATH):
   print 'base dir', BASEPATH, 'does not exist'
   sys.exit(1)

BASEPATH = os.path.abspath(BASEPATH)

print 'using base dir:', BASEPATH

sys.path.append(BASEPATH)
from common import *

LOW_FREQ = '2000000'
if len(sys.argv) > 2:
   mp = { '2.4' : '2400000', '2.2' : '2200000', '2.1' : '2100000',
          '2.0' : '2000000', '1.8' : '1800000', '1.6' : '1600000' }
   LOW_FREQ = mp[sys.argv[2]]

print 'Executing frequency:', LOW_FREQ

# parameters
PENALTY           = 100       # switching cost in us (included in timing information)
STDEV_THRESHOLD   = 0.01      # relative stdev threshold, should be 0.01 - 0.20
SKEW_CUTOFF       = 3.0       # maximum allowed skewness
BRIDGE_THRESHOLD  = 2000      # allowed (possible) CPU-intensive region gap
SHORT_REGION      = 200       # period when to collect anyway, as communication dominates
                              #   barrier time is 40-90us
TIME_THRESHOLD    = 1000      # cut-off to accept a cluster for scaling
if LOW_FREQ in ['2400000', '2200000', '2100000']:
   SAFETY_CUTOFF  = 1.5
elif LOW_FREQ in ['2000000', '1800000']:
   SAFETY_CUTOFF  = 1.8
elif LOW_FREQ in ['1600000']:
   SAFETY_CUTOFF  = 2.0

if len(sys.argv) > 3:
   BRIDGE_THRESHOLD = int(sys.argv[3])
if len(sys.argv) > 4:
   SHORT_REGION      = int(sys.argv[4])
print 'Using cut-offs (BRIDGE_THRESHOLD, SHORT_REGION):', BRIDGE_THRESHOLD, SHORT_REGION

CLUSTER_CUTOFF = 0
if len(sys.argv) > 5:
   CLUSTER_CUTOFF = TIME_THRESHOLD
   print 'CLUSTERING DISABLED!'

input = open(os.path.join(BASEPATH, 'stdevs.pkl'), 'rb')
data = cPickle.load(input)

# selected frequencies for comparison
ONLINE = FREQUENCIES[FREQUENCIES.index('default')]
LOW    = FREQUENCIES[FREQUENCIES.index(LOW_FREQ)]

ONLINE_DATA = data[ONLINE]
LOW_DATA    = data[LOW]

NREGIONS = len(ONLINE_DATA.stdevs)

class result(object):
   def __init__(self):
      self.energy = 0.
      self.time   = 0.

default = result()
lowered = result()

#print NREGIONS
#print len(ONLINE_DATA.regions)
#print sum(ONLINE_DATA.regions)/1.E6
#print sum(ONLINE_DATA.averages)/1.E6
#
#print
#
#print len(LOW_DATA.regions)
#print sum(LOW_DATA.regions)/1.E6
#print sum(LOW_DATA.averages)/1.E6


decisions = dict()
contextAvgs = dict()

class Statistics(object):
   def __init__(self):
      self.switches = 0

   def switch(self):
      self.switches += 1

   def switching_cost(self):
      return self.switches * PENALTY

stats = Statistics()

runningDecisions = list()

class DecisionState:
   CLUSTERING = 'C'
   SCALING    = 'S'
   BRIDGING   = 'T'

   def __init__(self):
      self.reset(0)

   def reset(self, context):
      self.state      = self.CLUSTERING
      self._decisions = dict()
      self._dtime     = 0.
      self._bridge    = dict()
      try:
         del decisions[context]
      except KeyError:
         pass

   def collect(self, context, dtime):             # accept a noisy region
      if self.state == DecisionState.BRIDGING:
         self._dtime = 0  # start of a potential new cluster
         self.state = DecisionState.CLUSTERING
      if self.state   == DecisionState.CLUSTERING:
         return self.cluster(context, dtime)
      elif self.state == DecisionState.SCALING:
         return self.scale(context, dtime)
      assert not "Unknown state!"

   def reject(self, context, dtime):              # accept a non-noisy region
      if SHORT_REGION < dtime:
       # long(ish) region, still if scaling already allow possible extension
         if self.state == DecisionState.SCALING:
            self.state = DecisionState.BRIDGING
         if self.state == DecisionState.BRIDGING:
            return self.bridge(context, dtime)
         return self.reset(context)
      else:
       # short region, so communication dominates: collect anyway
         return self.collect(context, dtime)

   def cluster(self, context, dtime):
      self._dtime += dtime
      self._decisions[context] = 1

      if TIME_THRESHOLD < self._dtime:
         self.state = DecisionState.SCALING
         return self.scale(context, dtime)

   def scale(self, context, dtime):
      if self._decisions:
         self._dtime = 0
         decisions.update(self._bridge)
         decisions.update(self._decisions)
         self._decisions = dict()
         self._bridge    = dict()
      decisions[context] = 1

   def bridge(self, context, dtime):
      self._dtime += dtime
      self._bridge[context] = 1
      if BRIDGE_THRESHOLD < self._dtime:
         return self.reset(context)

dstate = DecisionState()

#
## replay decision simulation
#

print 'Now replaying decisions ... '
for iregion in xrange(NREGIONS):
   try:
      schedule = decisions[ONLINE_DATA.contexts[iregion]]
      assert schedule == 1
      runningDecisions.append( schedule )
   except KeyError:
      runningDecisions.append(0)

   isSafe = True
   try:
      ref_avg = contextAvgs[ONLINE_DATA.contexts[iregion]]
      if runningDecisions[-1]:
       # use low frequency data as simulated scheduled for online
         isSafe = (LOW_DATA.averages[iregion]/ref_avg) < SAFETY_CUTOFF
   except KeyError:
      contextAvgs[ONLINE_DATA.contexts[iregion]] = ONLINE_DATA.averages[iregion]

   if isSafe and ONLINE_DATA.stdevs[iregion] > STDEV_THRESHOLD and ONLINE_DATA.skews[iregion] < SKEW_CUTOFF and ONLINE_DATA.regions[iregion] > CLUSTER_CUTOFF:
      dstate.collect(ONLINE_DATA.contexts[iregion], ONLINE_DATA.averages[iregion])
   else:
      dstate.reject( ONLINE_DATA.contexts[iregion], ONLINE_DATA.averages[iregion])

default.energy = ONLINE_DATA.power[0] * ONLINE_DATA.regions[0]
for iregion in xrange(1, NREGIONS):
   default.energy += ONLINE_DATA.regions[iregion]*(ONLINE_DATA.power[iregion]+ONLINE_DATA.power[iregion])/2.
default.time   = sum(ONLINE_DATA.regions)


#
## simulate energy assignment
#

class VoltageState(object):
   HIGH = 1
   LOW  = 0

   def __init__(self):
      self.state   = VoltageState.HIGH

   def down(self):
      if self.state == VoltageState.HIGH:
         self.state = VoltageState.LOW
         stats.switch()

   def up(self):
      if self.state == VoltageState.LOW:
         self.state = VoltageState.HIGH
         stats.switch()

   def isUp(self):
      return self.state == VoltageState.HIGH


print 'Simulating online assignment ...'
vstate = VoltageState()
lowered.energy = ONLINE_DATA.power[0] * ONLINE_DATA.regions[0]
lowered.time   = ONLINE_DATA.regions[0]
frac_lower_time = 0.
for iregion in xrange(1, NREGIONS):
   if runningDecisions[iregion]:
      vstate.down()
      lowered.energy += LOW_DATA.regions[iregion]*(LOW_DATA.power[iregion]+LOW_DATA.power[iregion])/2.
      lowered.time   += LOW_DATA.regions[iregion]
      frac_lower_time += ONLINE_DATA.regions[iregion]
   else:
      vstate.up()
      lowered.energy += ONLINE_DATA.regions[iregion]*(ONLINE_DATA.power[iregion]+ONLINE_DATA.power[iregion])/2.
      lowered.time   += ONLINE_DATA.regions[iregion]

# incorporate costs:
extra_energy = lowered.energy / lowered.time * stats.switching_cost()
extra_time   = stats.switching_cost()

fraction = 100.*frac_lower_time/default.time
savings  = -((lowered.energy + extra_energy ) / default.energy - 1. )*100.
cost     = -((lowered.time   + extra_time   ) / default.time   - 1. )*100.

print '   totals  :', lowered.time/1.E6, default.time/1.E6
print '   fraction:', fraction
print '   savings :', savings
print '   cost    :', cost

print
print 'cross-check: # barriers:', len(LOW_DATA.regions)
print '             total time:', sum(LOW_DATA.regions)/1.E6
print '           rel. savings:', -(sum([x*y for x,y in zip(LOW_DATA.power, LOW_DATA.regions)])/sum([x*y for x,y in zip(ONLINE_DATA.power, ONLINE_DATA.regions)])-1)*100
print '                  @cost:', -(sum(LOW_DATA.regions)/sum(ONLINE_DATA.regions)-1)*100
print '             # switches:', stats.switches

print ' --- '
print '% 5.1f%%  /% 6.1f%%   /% 9d     /% 9.1f%%' % (savings, -cost, stats.switches, fraction)
