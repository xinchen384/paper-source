import cPickle, glob, math, os, sys
from dataobjects import *

BASEPATH = sys.argv[1]
if not os.path.exists(BASEPATH):
   print 'base dir', BASEPATH, 'does not exist'
   sys.exit(1)

BASEPATH = os.path.abspath(BASEPATH)

print 'using base dir:', BASEPATH

sys.path.append(BASEPATH)
from common import *


# Format for run1:
#
# context:barriertime:regiontime:power:<online decision>
#
# context = hash of stack trace, uniquely defines barrier start
# barriertime = for pair (B1, B2), time from end of B1 to start of B2
# regiontime = for pair (B1, B2), time from end of B1 to end of B2
#  (by definition: regiontime > barriertime)
# power = current power in Watt
# <online decision> = only for online code:
#   decision value to follow the simulation
#   -1 : no decision yet
#    0 : no frequency switch at all
#    1 : no frequency switch b/c of time threshold (overriding analysis)
#    2 : frequency switch
#   10 : added each time changed mind


def collect_nids():
   nids = []
   os.chdir( os.path.join( BASEPATH, FREQUENCIES[0], 'run1' ) )
   for lfile in glob.glob( "*.lock" ):
      nids.append(lfile[3:-5])
   return nids
NIDS = collect_nids()

def collect_coreids():
   coreids = []
   os.chdir( os.path.join( BASEPATH, FREQUENCIES[0], 'run1' ) )
   for lfile in glob.glob('LOG_nid'+NIDS[0]+'_*_*.log.gz'):
      coreids.append(lfile[lfile.rfind('_')+1:-7])
   coreids.sort()
   return coreids
COREIDS = collect_coreids()

# Data collection:
#
# For default run1, collect the barrier times & contexts per rank.

data = {}

region_infos = list()

for freq in ['default']:
   data[freq] = dict()

   print 'processing', freq
   FREQUENCY = data[freq]
   os.chdir( os.path.join( BASEPATH, freq ) )

   RUNS = glob.glob('run?')
   RUNS.sort()
   for r in RUNS[:]:
      if r in SKIPRUNS[freq]:
         RUNS.remove( r )
   NRUNS = len(RUNS)

   for runid in [RUNS[0]]:         # only 1 run
      os.chdir( os.path.join( BASEPATH, freq, runid ) )

      barrier_times    = list()
      barrier_contexts = list()

      for inid in range(len(NIDS)):

         nid = NIDS[inid]
         print '... processing runid:', runid, 'nid:', nid, '(%d)' % (inid+1)

         for coreid in COREIDS:

            print '   ... processing coreid:', coreid

            logfiles = glob.glob('LOG_nid'+nid+'*_'+coreid+'.log.gz')
            assert len(logfiles) == 1

            contexts = list()
            barriers = list()
            for l in os.popen('gunzip -c ' + logfiles[0]).readlines():
               if l[0] == '#':
                  break             # done with regions data
               l = l.split(':')
               contexts.append(int(l[0]))
               barriers.append(float(l[1]))

            barrier_contexts.append(contexts)
            barrier_times.append(barriers)

      for iregion in xrange(len(barrier_times[0])):
         r = region_info()
         r.barriers.append(barrier_times[0][iregion])
         r.context = barrier_contexts[0][iregion]
         region_infos.append(r)

      for iregion in xrange(len(barrier_times[0])):
         for irank in xrange(1, len(barrier_times)):
            region_infos[iregion].barriers.append(barrier_times[irank][iregion])
            # presume contexts is always the same (not true)

import cPickle
os.chdir(BASEPATH)
output = open("balance.pkl", "wb")
cPickle.dump(region_infos, output)
