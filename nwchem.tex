\subsection{Application Benchmarks}
\label{sec:nwchem}

%NWChem~\cite{journals2010nwchem} is a computational chemistry code widely used in the scientific community. The portability and the scalability are emphasized on the implementation of Quantum Mechanics and Molecular Mechanics methods. The code base contains highly modular software written in multiple programming languages and composes multiple runtimes and paradigms. The source code has more than 6 million of lines written in C, C++, Fortran and Python. Multiple software modules constitute a complicated runtime infrastructure that supports for dynamic tasking, load balancing, memory management, resiliency, one-sided communication and synchronization. We believe these are all characteristics of future scientific codes for the Exascale era. 

{\bf NWChem}~\cite{valiev} delivers open source computational chemistry software to a large user community.  It uses Coupled-Cluster (CC) methods, Density Functional Theory (DFT), time-dependent DFT, Plane Wave Density Functional Theory and Ab Initio Molecular Dynamics (AIMD).
It contains roughly 6M lines of code.  
The main abstraction in NWChem is a globally addressable memory space provided  by Global Arrays~\cite{Nieplocha}. The code uses both one-sided communication paradigms (ARMCI~\cite{armci}, GASNet~\cite{gasnet}, ComEx~\cite{comex}, MPI 3.0 RMA), as well as two sided communication (MPI).  Most methods implement a Read-Modify-Update cycle in the global address space, using logical tasks that are dynamically load balanced. Communication is aggressively overlapped with other communication and computation operations. The code also provides its own resilience approach using application level checkpoint restart, thus file I/O dominates parts of the execution.

With two sided MPI as the transport layer, the code performs a sequence of overlapped {\tt Isend} | {\tt Probe} | {\tt Recv(ANY\_SOURCE) }operations. With ARMCI as the transport layer, the code runs in an asymmetric configuration where proper ``ranks'' are supplemented with progress threads that perform message unpacking and Accumulate operations, driven by an interrupt based implementation.  


We have chosen NWChem since no previous work handled dynamic load balance mechanisms, nor one-sided communication in large scale application settings. Furthermore, note that the ARMCI back-end is challenging due to its asymmetric runtime configuration using progress threads. We have experimented with both MPI and ARMCI. For brevity we concentrate on presenting ARMCI results, see Section~\ref{sec:ts} for MPI.

For this paper we experiment with the CC and DFT methods, as these account for  most usage of  NWChem.  CC is set to run with and without I/O (writing partial simulation results to disk for resilience reasons).
We used two  science production runs: simulation of the photodissociation dynamics and thermochemistry of the dichlorine oxide $(Cl_{2}O)$ molecule, and of the core part of a large metalloprotein.



\parah{Dynamic Behavior of NWChem} 
Slack is virtually non-existent, as a result of dynamic load balancing
in NWChem.
When running on 1,034 cores on Edison, a very large fraction of regions (more than 50\%) are very
short, on the order of several tens of $\mu s$. On the other hand, these short
regions account for at most 10\% of the total execution time and need to be
clustered to prevent excessive frequency switching. For
reference, the program executes 204,452 barriers during this run.
The most relevant regions for DVFS are longer (in the few $ms$ range) and
repeat only about 10 times, which
indicates that online learning needs to be fast.
This also means that an energy optimization approach needs to be able to handle both short execution regions, as well as long running program regions. As already shown, predictions of execution time between 
synchronization operations in NWChem are likely to fail.
We examine each method at increasing concurrency, up to 1,034 cores. CC  executions exhibit different  dynamic behavior with increased concurrency. DFT execution is CPU-intensive.
\\

{\bf UMT2K}~\cite{umt} is a 3D, deterministic, multigroup, photon transport code for unstructured meshes~\cite{umt}. It is written in MPI and OpenMP and it provides its own checkpoint/restart mechanism. We have chosen this benchmark as it provides a common reference point with state-of-the-art quantitative approaches able to handle MPI codes. UMT2K contains roughly 170K lines of code.

\parah{Dynamic behavior of UMT2K} The benchmark is CPU-intensive and performs blocking MPI communication, i.e. no overlap.
Communication regions account for about 20\% of the execution time.
It is  largely load balanced, with some  communication regions
having increased work for MPI Rank $0$.
The time intervals between two collective operations are large ($\approx 250ms$), the
benchmark performs about ~1000 {\tt Barrier} and {\tt Allreduce} operations
each over a 4 minutes long execution. Overall, UMT2K is reasonably well balanced with coarse grained 
computation between collective operations.

