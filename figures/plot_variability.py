from ROOT import *

#gROOT.SetBatch()
gStyle.SetOptStat("011")

textsize = 0.042

f = TFile("variability.root")
h = f.variability

c = TCanvas("vari", "", 600, 424)

h.SetTitle("")
h.GetXaxis().SetTitle("first occurence relative standard deviation")
h.GetXaxis().SetTitleOffset(0.9)
h.GetXaxis().SetTitleSize(textsize)
h.GetXaxis().SetLabelSize(textsize)
h.GetYaxis().SetTitle("subsequent occurences rel. std. dev.")
h.GetYaxis().SetTitleOffset(0.7)
h.GetYaxis().SetTitleSize(textsize)
h.GetYaxis().SetLabelSize(textsize)
h.SetMarkerStyle(7)
h.Draw("")

c.Modified()
c.Update()

q = raw_input("print(y/n)?")
if q == 'y':
   c.Print("variability.pdf")
