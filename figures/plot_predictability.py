from ROOT import *

#gROOT.SetBatch()
gStyle.SetOptStat("011")

textsize = 0.09

f = TFile("balance.root")

c = TCanvas("pred", "", 600, 400)

c.Divide(1,2)

def drawme(h):
#  h.Rebin(4)
  h.SetTitle("")
  h.GetXaxis().SetTitle("ranking percentile (%)")
  h.GetXaxis().SetTitleOffset(0.9)
  h.GetXaxis().SetTitleSize(textsize)
  h.GetXaxis().SetLabelSize(textsize)
  h.GetYaxis().SetTitle("probability (%)")
  h.GetYaxis().SetTitleOffset(0.33)
  h.GetYaxis().SetTitleSize(textsize)
  h.GetYaxis().SetLabelSize(textsize)
  h.SetMinimum(0)
  h.DrawNormalized("", 100)

c.cd(1)
drawme(f.critical)
a = TPaveText( 60, 4, 80, 5.5, "NB" )
a.SetTextSize( textsize )
a.SetFillColor( 0 )
a.AddText("critical path")
a.Draw()

#c.Modified()
#c.Update()

c.cd(2)
drawme(f.fastest)
b = TPaveText( 60, 3, 80, 4, "NB" )
b.SetTextSize( textsize )
b.SetFillColor( 0 )
b.AddText("fastest path")
b.Draw()


f.critical.GetXaxis().Draw()

c.Modified()
c.Update()

q = raw_input("print(y/n)?")
if q == 'y':
   c.Print("predictivity.pdf")
