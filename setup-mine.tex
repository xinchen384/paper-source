\section{Evaluation}
\label{sec:eval}

We evaluate the efficacy of our optimizations on the NWChem and UMT2K
applications described in Section~\ref{sec:nwchem}. We compare the
online and offline optimization approaches using up to 1,034 cores on two clusters and one
large scale HPC production system. For completeness we have attempted to
compare against MPI based quantitative approaches,
Adagio~\cite{adagio} and the clustering~\cite{mpicluster}  algorithm
used by Lim et al. The results are described in Section~\ref{sec:ts}. 



\subsection{Experimental Methodology}
\label{sec:methodology}

\parah{Platforms} The Teller~\cite{teller} cluster has four AMD
A10-5800K quad-core processors and 16GB main memory per node. There
are seven available frequencies for scheduling, ranging from 1.4~GHz
to 3.8~GHz. Each core can be individually controlled; in the idle
state cores consume 45W, 55W at 1.4~GHz and 110W at 3.8~GHz.  Each
node runs on Red Hat Enterprise server 6.2 and Linux kernel 2.6.32,
and the frequency switching is implemented on top of the {\tt
cpufreq}~\cite{cpufreq} library.  The Edison~\cite{edison} Cray XC30
system at NERSC~\cite{nersc} is a large scale production system, with
two 12-core Intel Ivy Bridge 2.4~GHz processors per node and 5,576
total nodes, 133,824 cores in total.  Frequencies can be set in
increments of 0.1~GHz from 1.2~GHz to 2.4~GHz, only at socket granularity.
At idle cores consume about 65W, to 115W at 1.2~GHz and up to 250W
at 2.4~GHz.
Shepard is a 36 node cluster with dual socket Intel
Haswell 16-core processors. This processor is to be deployed in the
next generation large scale systems at DOE Labs~\cite{cori,trinity} in
early 2016. The frequency on Haswell can be selected per core, from
1.2~GHz to 2.3~GHz. However, cores share many resources, such as the
L2 ring which are not affected until all cores in the socket scale
down. Additionally, on Shepard the Mellanox InfiniBand FDR driver
requires a single frequency to be set across all cores within the
node. At idle the cores draw 76W, 104W at 1.2~GHz, 150W at 2.3~GHz and
220W in Turbo mode.

\parah{Methodology} We measure at-the-wall power consumption, this being the ultimate indicator of any energy savings.  We use micro-benchmarks to determine the DVFS latency to calibrate  our optimizations.
On Teller and Shepard we use the PowerInsight~\cite{laros2013} interface  to collect power data, which is integrated with the application level  time stamps  to yield energy consumption.
The DVFS switching latency is $\approx 100 \mu s$ on Teller and
Shepard~\cite{hackenberg2015}.
Results reported for Teller and Shepard are from runs using our
optimizations during the application execution.

On Edison power is measured  by the Cray power monitoring counters,
which sample at a frequency of about 10 Hz, and which can be
read from a virtual filesystem under {\tt /proc}.
As Edison is a large scale production system, the only DVFS control
allowed is at job startup time, when a single frequency can be selected.
Therefore, results on Edison are estimated using modeling on timing and trace data.
We run the application at  all available frequency steps, including
default Turbo mode. To account for optimization overhead, the
algorithm is executed during each run, all except the final DVFS calls. To account for DVFS switching overhead we use
$\approx 100 \mu s$ delays, which is a conservative
estimation~\cite{mazouz2014} for
this system.
For any frequency, we build trace files obtained by averaging
on each task the duration of a region across at least five runs. 

As an extra validation step for the Edison results, we have compared
the clusters selected by the model with the clusters selected
by the online algorithm when running on Teller and Shepard  at similar
concurrency. There is a very high overlap in selected regions, which
confirms that we do indeed select during simulation the portion of the execution
amenable to DVFS due to contended hardware resources.
 
\subsection{Tuning Parameters}
\label{sec:freq}

On each system, we select the high and low frequency thresholds using the performance micro-benchmarks described in
Section~\ref{sec:criteria}.

For the {\it high frequency} we have a choice between Turbo mode and the
highest static  frequency available.  On HPC production systems,
the Turbo mode is enabled by default
as it usually attains the best performance: this is the case  on Teller and Edison.
For reference, 2.4~GHz is the highest possible static frequency on Edison, but
setting it explicitly will switch off Turbo-boost and DVFS by the hardware.
In comparison to static 2.4~GHz, Turbo gains $8\%$ in performance at
a cost of $13\%$ in energy \red{\bf  for the input blah.}
On Shepard, best performance is obtained at a static frequency of 2.3~GHz due to
performance bugs in the Mellanox InfiniBand driver, uncovered during this work.
In Turbo mode, communication is significantly slower than at static 2.3~GHz. The
driver expects that all cores are set at the same frequency as it reads the
current CPU configuration file and uses it throughout to estimate delays. Turbo
mode is logically denoted by the 2.301~GHz frequency, while in reality the cores
will run anywhere between 2.8~GHz-3.4~GHz, depending on load. In our experiments we
have  modified the driver to configure  the NIC with the correct high Turbo
frequency. This recouped some of the loss, but a vendor fix is required before
Turbo mode can match the performance at 2.3~GHz.

Note that the existence of a Turbo mode, with different top frequencies
depending on the workload, and thus the lack of a static reference frequency
poses problems to all MPI quantitative approaches\cite{rountree2011}.


\begin{figure}[h]
% \centering
 \includegraphics[scale=0.55]{figures/energy_vs_freq_haswell.pdf}
\caption{\label{fig:ef_haswell} \footnotesize\textit{Relative energy usage
(1.8~GHz $==$ 1) for micro benchmarks on Shepard that are CPU, memory, network,
and file access limited, respectively.
Network results are before driver fixes, and using all cores.
Arrows indicate the lowest frequency possible while remaining time optimal.}}
\vspace{-0.2in}
\end{figure}

We select the {\it low frequency} based on the energy cost of codes that are
limited by specific resources.
Figure~\ref{fig:ef_haswell} shows such experimental results on Shepard.
Clearly, different resources are impacted differently, thus the choice for
the low frequency determines what regions we want to select for DVFS, and that
means it determines the dispersion and skewness cut-offs.
In other words, the parameter selection is over-constrained, and it takes only
a few iterations to arrive at an optimal set.

For example, start with a low frequency of 2.1~GHz.
This scaling affects network- and CPU-bound codes, and the latter the most.
Next, create dispersion and skewness plots for codes for different mixtures of
part CPU- and network-bound, part not.
See for example Figure~\ref{fig:cpumemnoise} where we have done this for Edison.
Scaling down to 2.1~GHz for CPU-bound code incurs a cost of 33\% (assuming most
cores are in use, i.e.\ an effective Turbo frequency of 2.8~GHz).
The effect of intra-node communication is the same, the effect of inter-node
communication is a speed-up, but only marginally so after our driver fixes.
Assume in the first iteration that 20\% of the program time will be scaled.
Thus, the cost could be 20\% of 33\%, or 6.6\%. We want to bound this by 2\%,
so the scaled regions may not be more than 30\% CPU- or network-bound.
We can now read the dispersion and skewness cut-offs directly from their
respective plots at the cross of 30\% and the desired scale.
We run a test, verify the 20\% assumption, and iterate with an improved
selection.\footnote{This assumes that there is a wide distribution in
mixtures across regions. True in large, complex applications such as NWChem,
but not in highly synchronized or simple codes.}
Likewise, adjust the choice for low frequency if beneficial.
Within a few iterations we attain an optimal set of parameters.

Since the parameter set is over-constrained, we can make trade-offs.
For example, choosing a higher low frequency allows looser cut-offs, and
thus a greater selection of program regions.
The higher frequency results in lower savings, but these are applied to a
larger portion of the program, netting the same result: the algorithm is
very robust over a range of parameters.
However, the trade-off between performance and energy is not a linear
function of the CPU frequency for most resources: the largest gains are
had by coming down from the highest frequencies, with only small gains as
we approach the frequency at which the memory runs.
We therefore choose a frequency a little higher than memory and we end
up with \red{\bf 3.4~GHz for Teller}, 2.1~GHz
for Edison, and 2.1~GHz on Shepard for the respective low frequencies.

Finally, we need parameters for clustering.
We choose the {\tt SHORT} threshold to be $5 \times$ the communication
cost of a {\tt barrier}, and the {\tt BRIDGE} cutoff to be $10 \times$ the
cost of DVFS latency (i.e.\ the expected average bridge size would be half
that, thus balancing the cost of DVFS against the cost of scaling a
fully CPU-bound region). The actual values chosen need not be highly
tuned, and we don't change them with scale.  The reason is interaction
of the different parts of the algorithm.  For example, choose the {\tt
SHORT} cutoff too tight, and bridging will cover most cases anyway.
Likewise, the purpose of collecting short regions is not so much to
increase the size of clusters (short regions have little impact in
overall energy savings or program performance), but rather to allow
building longer bridges in parts of the program that are dominated by
communication, and hence to save on frequency switching.

\input{nwchem}
