


\subsection{Impact of Algorithmic Choices}

For brevity, we do not discuss the Teller  results in
detail. The Edison and Shepard CPUs are one and two generations ahead
respectively, have better hardware power management and
energy optimizations on these platforms are more challenging. For
reference, on Teller for CC we observe as much as 7.4\% energy savings for a 1.7\%
slowdown provided by an online optimization that uses 3.4~GHz as the
target frequency, running on 128 cores.

\comment{
\begin{figure*}[htbp!]
\begin{tabular}{cc}
\begin{minipage}{3.2in}
\includegraphics[width=3.2in,height=1.2in]{cc-edison-evss.pdf}
 \caption{\label{fig:ccevss}\small\textit{Summary of results on Edison
   for CC running at increasing concurrency (132,518,1034) and with
   different target low frequency for the online algorithm. }} 
\end{minipage} &
\begin{minipage}{3.2in}
\includegraphics[width=2.5in,height=1.5in]{cc-edison-evss.pdf}
 \caption{\label{fig:ccevss}\small\textit{Summary of results on Edison
   for CC running at increasing concurrency (132,518,1034) and with
   different target low frequency for the online algorithm. }} 
\end{minipage} \\
\begin{minipage}{3.2in}
\includegraphics[width=2.5in,height=1.5in]{cc-edison-evss.pdf}
 \caption{\label{fig:ccevss}\small\textit{Summary of results on Edison
   for CC running at increasing concurrency (132,518,1034) and with
   different target low frequency for the online algorithm. }} 
\end{minipage} \\
\end{tabular}
\end{figure*}
}

We'll concentrate our interest on Edison, as this is a tightly
integrated production HPC system. Figure~\ref{fig:ccevss} presents results for the online optimizations
of the CC run. The labels contain the concurrency, e.g. CC-1034 refers
to a run on 1,034 cores. For each configuration we allow the low
frequency to take the values indicated on the $x-axis$. For reference,
the runs perform 75,233, 75,085 and 204,452 regions when running on
132, 528 and 1,034 cores respectively. These occur in roughly 6,000 to
9,000 distinct calling contexts, depending on the input. The typical execution is on the
order or 20 minutes or more.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=3.2in]{cc-edison-evss.pdf}
 \caption{\label{fig:ccevss}\footnotesize\textit{Summary of results on Edison
   for CC running at increasing concurrency (132,518,1034) and with
   different target low frequency for the online algorithm. }}
\end{center}
\end{figure}

 The execution of CC-132 is memory bandwidth limited
and we observe energy savings as high as 7.8\% for a slowdown of 3.9\%
when varying to 1.6~GHz. When using the ``default'' target 2.1~GHz
frequency, the optimization saves 6.5\% energy for a 1\% slowdown. A
low frequency is selected for about 25\% of the total program
execution using $\approx 3,600$ DVFS switches.  The execution of
CC-528 is dominated by a combination of memory intensive execution and
communication. The energy savings are as high as 8.3\%, for a slowdown
of only 0.8\%. Low frequency is selected roughly for 33\% of the
execution, using $\approx 8,000$ switches.
The execution of CC-1034 is dominated by a combination of I/O and
communication. In the best case we observe savings of 4.4\%  with a 1.5\% slowdown. Low frequency is selected for about 23\%
of the execution, using $\approx 70,000$ switches. 


Figure~\ref{fig:ccevss} also illustrates that most of the benefits are
obtained when lowering the frequency to 2~GHz or 2.1~GHz, close to the
memory frequency but not lower. This validates our choice of
considering only one target frequency and in practice we use 2.1~GHz
as default.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=3.2in]{cc-edison-algimp.pdf}
 \caption{\label{fig:ccalgimp}\footnotesize\textit{Impact of algorithmic
     design choices on the efficacy of the online algorithm using a
     target frequency of 2.1~GHz on Edison.}} 
\end{center}
\end{figure} 

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=3.2in]{cc-edison-algcov.pdf}
 \caption{\label{fig:ccalgcov}\footnotesize\textit{Percentage of the
     execution time at low frequency as determined by algorithmic
     choices on Edison using a target of  2.1~GHz.}}
\end{center}
\end{figure}


Figures~\ref{fig:ccalgimp}~and~\ref{fig:ccalgcov} provide some
quantitative detail about the influence of our algorithm design
choices. Overall, they illustrate the fact that both clustering and the
variability criteria are required in practice to cover the spectrum of
code dynamic behavior. We present energy savings, slowdown and execution coverage
for multiple algorithms, compared to the complete online algorithm
labeled ALL. Each label designates the criteria we subtract from the
full algorithm in a progressive manner: ``-SUPER'' denotes lack of forming super-clusters
(i.e.\ ${\tt BRIDGE} = 0$ Algorithm~\ref{alg:clustering}),
``-SHORT'' denotes ignoring short regions (i.e.\ ${\tt SHORT} = 0$), ``-CLUSTER'' denotes no
clustering at all. Finally ``-STDDEV'' denotes an algorithm that
ignores completely variability, but still cuts on skewness.  For reference, the series labeled
``-SHORT'' (i.e.\ no refinement beyond clustering) is the equivalent
of the algorithm presented by Lim et al~\cite{mpicluster}. 
 

For CC-132 and CC-528, most of the benefits are provided by the
clustering, rather than the variability selection
criteria. Also note that forming super-clusters is mandatory for
performance, as illustrated by the increase in energy savings and
decrease in overhead for ``-SUPER'' when compared to ``-SHORT''  for
CC-132 and CC-528. When increasing concurrency, the variability
selection criteria provides most of the benefits of the optimization,
and clustering improves behavior only slightly.  Similar conclusions
can be drawn  when examining the execution coverage in Figure~\ref{fig:ccalgcov}, rather than
performance improvements. 

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=3.2in]{opt-all.pdf}
 \caption{\label{fig:optall}\footnotesize\textit{Comparison of  energy
     savings and slowdown for CC, CC-IO, DFT and CC-MPI when using
     online and offline optimizations on Edison with a target low
     frequency of 2.1~GHz.}} 
\end{center}
\end{figure} 

For brevity, we did not provide detailed results for all
benchmarks as they show similar trends to the CC runs.  In
Figure~\ref{fig:optall} we show results for runs of CC-IO, which runs
the resilience methods, DFT and selected configurations running on
MPI. For CC-IO we observe even better energy savings than for CC, due
to extra optimization potential during I/O operations. 
The DFT execution is flops-limited and as already described in
Section~\ref{sec:criteria} our approach does not identify many regions as
DVFS candidates. MPI results are further discussed in Section~\ref{sec:ts}. 

On Shepard, like on Teller, results are obtained with DVFS during
application execution and summarized in Figure~\ref{fig:shepard_results}.
For CC, we used molecules of increasing sizes as input: $(Cl_{2}O)$
(OCT)  and $(C_{4}H_{6}N_{3}O_{2})$ (DCO). As shown, we observe as
much as 12\% energy savings with in most cases only a small performance
penalty.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=3.2in]{shepard_results.pdf}
 \caption{\label{fig:shepard_results}\footnotesize\textit{
     Comparison of  energy savings and slowdown for CC (a small and
     large molecule), at different low frequencies and with and
     without clustering of short regions.}}
\end{center}
\end{figure}

As we scale up the small molecule input, it becomes increasingly dominated
by communication, which is CPU-dependent if intra-node.
As a result, ``blindly" accepting short regions carries an increasingly
large cost, requiring {\tt SHORT} set to $0$ to get good results.

For DFT, timings are dominated by file
access on Shepard (even at small scale) and their range is huge (up to 4x), putting doubt in the usefulness
of the results.
Nevertheless, we ran a large number of jobs and averaged them, achieving
savings of 9\% for no appreciable loss in performance.
That result is encouraging, but much larger savings can be had by
installing a better I/O  system.

As expected, we did not achieve energy savings on the UMT2K benchmark
on Shepard, nor did we slow it down. The load balanced parts are
CPU-intensive and rejected by our criteria. Some communication regions
have increased work for Rank 0: the imbalance is within our threshold
and the algorithm decides to apply DVFS. However, at subsequent
executions of the context the variation in the execution time of Rank
0 is too large and the algorithm disables DVFS.
Compared to 9000 in NWChem, the dynamic behavior is much simpler: we observe
only 40 distinct calling contexts.

\subsection{Optimality of Online Algorithm}

The online algorithm observes the
execution of any region for a small number of repetitions and if
necessary varies the frequency to a unique predetermined value. This leaves
untapped optimization potential. In Figure~\ref{fig:optall} we
include for reference  the energy savings and the slowdown for the offline trace
based optimization approach which chooses the optimal frequency for any given
cluster. As illustrated, the offline approach almost doubles the
energy savings for similar or slightly lower runtime overhead. In
particular,  we now observe 11\% savings at 1,034 cores for
CC-1034. More than half  of the additional energy gains come from the ability to perform
DVFS on the first occurrence of any region, rather than from choosing
the optimal frequency assignment for a particular region. To us, this
validates the algorithm robustness and relative lack of sensitivity to
the selection of the low frequency threshold; it is better to act fast
but imprecise, rather than wait for enough data for statistical
significance to compute an optimal solution.




\subsection{Impact of Communication Paradigm}
\label{sec:ts}
The results presented so far have been for NWChem configurations using
one-sided communication. For brevity we offer a summary of our
findings when running NWChem configured to use MPI.
First, the  application runs  slower, and we have observed more than $4 \times$ slowdown on
Edison when comparing MPI with ARMCI performance on 1,034 cores. A large contribution
is attributed to slower communication, which implies larger possible
gains from DVFS. Due to the two-sided nature, instrumenting at barrier
granularity shows that the code exhibits less imbalance than runs with ARMCI, as induced by
the multiple {\tt ISend/IRecv/Probe} operations performed between barriers.

With MPI we observe much higher energy savings (up to 20\% using offline analysis, up to
16\% using online), relative to the one-sided results.
The portion of the execution affected is also higher,
up to 50\%.  Since the MPI code is seemingly balanced, clustering provides most of the
optimization benefits, while the variability criteria provides a
safety valve.  This is in contrast with the one-sided behavior, where
recognizing variability is required at scale.
When compared to Lim's approach, we use different selection criteria when clustering, namely variability,
but after that, we do add back short regions, which is what their algorithm
uses for its clustering criteria.
For example, our full optimization (ALL) provides energy savings of 13\% with
a slowdown of 3.3\%, while an algorithm similar to Lim's that does pure
clustering (-SHORT) attains 9.3\% savings with 0.9\% slowdown.
Note that adding the (presumed communication bound) short regions (-SUPER) so
that the selection criteria are similar, too, results in 11.8\% savings, for
a cost of 2.9\%.

The network hardware also emphasizes the differences between 
communication paradigms. InfiniBand on Shepard has little hardware assist for
message injection, while Cray Aries~\cite{aries}  on Edison provides FMA and BTE
hardware support for small and large messages respectively. 

On
InfiniBand, one-sided  small transfers and collective operations are dominated by
CPU overhead:  our approach classifies them accordingly and does not
attempt DVFS. Indeed,
scaling down the frequency during these operations leads to execution
slowdown. Our approach classifies MPI two-sided transfers as amenable
to DVFS: lowering the frequency during these transfers does
not affect execution time.  Two-sided communication combines data transfer
and inter-task synchronization semantics:
here waiting and network latency dominates the message initiation CPU
overhead. 

On Edison, our criteria identifies both one-sided and two-sided small
transfers and collectives as amenable to DVFS: indeed this is the case
and lowering the frequency does not affect their performance. 

Overall, it seems that MPI codes exhibit more DVFS potential due to
their  over-synchronized behavior. This becomes apparent when strong
scaling NWChem on Shepard enough that messages become small.
Our method still finds DVFS opportunities, however, because file system
access is heavily contented at scale.


\comment{
Adagio - would very likely select the bottom line - you look for large difference between fast/slow.
They would prefer regions with large skew - more regions that we accept. Scaling down will have different effect - they keep adjusting.
We get 10 reps per region, need to learn fast and they vary a lot.

Cluster - need regions dominated by comm and typical comm time is 20-80us, minimum duration to be selected is 1ms. less 1% spent in comm.
They will never select these regions.

(Our first step is different - uses criteria...)
They accept everything that is short -
Our third step nobody does ...
}



\subsection{Comparison to Adagio}

\begin{figure*}[ht!]
\vspace{-0.7in}
\begin{minipage}[t]{\textwidth}
\begin{minipage}[c][1\width]{0.5\textwidth}
\centering%
\includegraphics[width=\textwidth]{adagio_shepard}
\end{minipage}
\begin{minipage}[c][1\width]{0.5\textwidth}
\centering%
\includegraphics[width=\textwidth]{varskew_shepard}
\end{minipage}
\vspace{-0.6in}
\caption{ \label{fig:adagio_varskew} \footnotesize \textit{\footnotesize The savings effect as a function of
imbalance for Adagio (left) and our algorithm (right) for a memory bandwidth
limited micro-benchmark. The smaller structures are an artifact of the plotting and limited sampling.}}
\end{minipage}
\end{figure*}

Adagio~\cite{adagio} provides state-of-the-art  scaling of MPI
applications using  a quantitative approach. To provide a fair comparison, 
we started with the official source~\cite{adagio-code}, fixed  the baked-in assumptions about
the hardware (e.g.\ four cores per node, no hyperthreads),  made the code ``hyperthread-aware", extended it to handle  Turbo,
optimized frequency setting, and built it in optimized (-O3) mode.
With that, we verified with the provided tests and under the same
original conditions of one MPI rank per DVFS/clock domain  that the algorithm indeed works as
expected. When applied to our test applications Adagio finds no energy saving potential in NWChem and
causes a 60\% slowdown (at an 28\% energy {\em increase)} in UMT2K, due to selecting a too low frequency
in communication regions, which are CPU sensitive on Shepard.

The results showcase the
differences between quantitative and qualitative approaches.
Adagio predicts the timing for each region and computes its ideal frequency as a linear combination of the discrete frequencies
available.  Frequency on each core is changed during a region using interrupts and this poses scalability problems with the number of cores. To amortize overheads, it requires relatively long regions 
(a multiple of 100ms)  executed at a given frequency. 
Such long region times do not occur often in NWChem: the typical region times are too short to even accept the overhead of
the additional frequency shifts, and clustering is necessary. Besides coarse grained regions, Adagio requires load imbalance between ranks proportional to 
the frequency differential between two consecutive assignments. 
When lacking an optimal choice, the algorithm  conservatively assumes a CPU-bound code, or a slowdown equal to the frequency
differential.
%%WLAV: I think we should use he lower (2.3/2.8-1) to (2.3/3.4-1) here, rather
%%      than (2.8/2.3-1) and (3.4/2.3-1)
The original Adagio implementation uses the highest static frequency as reference.
With our extensions to handle Turbo mode, the differential to next frequency is large and depends on the number of cores in use (e.g. from 18\% to 32\% on
Shepard), and none of our applications
exhibit imbalances of that magnitude.
Figure~\ref{fig:adagio_varskew} shows results for  a memory bandwidth limited micro-benchmark designed to favor Adagio 
with regions of several seconds long.
Adagio (left) works best when only a few ranks are slow (i.e.\ most cores can
be scaled down) and the imbalance is significant enough.
When most cores are slow, the energy savings effect of scaling down only a few
cores is negligible.
Our algorithm scales down more aggressively, which allows it to capture larger
savings even in much smaller regions, but conversely, it can not find savings
if there is significant imbalance.
Note that Adagio performs similarly for a CPU-limited micro-benchmark, whereas
our algorithm will not achieve any savings at all in that case. Contrarily, on Shepard where 
small to medium communication and collectives are CPU-bound, Adagio indiscriminately
scales down communication  resulting in large performance penalties for NWChem and UMT2K. 

\comment{Adagio also assumes that communication-heavy regions can be scaled down to
the lowest possible frequency.
But on Shepard, this only holds for largish inter-node messages or for those
cases where the network is congested, whereas for NWChem, communication partners
are random and for UMT2K, the messages are small.
Consequently that assumption does not hold and the performance penalty is severe.}
