\section{Design and Implementation}
\label{sec:design}
\label{sec:clustering}

We develop single-pass, online optimizations, as well as offline
optimizations.  The online approach exploits the iterative nature of
scientific codes: for each region first observe execution under
different frequencies, then decide the optimal assignment and apply it
when re-executed. Regions are distinguished in their calling context
using the program stack traces. The online optimization is built for
scalability and practicality, while we use the offline version to
understand and tune the online optimization engine.

\parah{Region Partition} In one sided communication, inter-task
synchronization is explicit and not associated with the regular {\tt
Put/Get} operations.  Often these codes use non-blocking communication
overlapped with independent computation, separated by group
synchronization operations, such as barriers, to maintain data
consistency.  To capture the dynamic nature of the execution we use a
context sensitive approach where we identify regions with a hash,
created from the return addresses of the full stack trace, at the
preceding collective operation.




%%%%%
\comment{
\begin{figure}[ht] \centering
 \includegraphics[scale=0.48]{./figures/design-figures/region.pdf}
\vspace{-0.15in}
 \caption{\small\textmd{{\red FIX THIS FIGURE, IT IS CONFUSING}}}
 \label{fig:region}
\end{figure}
\begin{figure*}[ht] \centering
 \includegraphics[scale=0.58]{./figures/design.pdf}
\caption{\small\textmd{REPLACE WITH SIMPLE STATE MACHINE.}}
 \label{fig:design} \vspace{-0.2in}
\end{figure*} }

%%%%%
%\subsection{Region Selection and Clustering}
%\label{sec:clustering}

\parah{Region Selection and Clustering} To estimate dispersion and
skewness, we replace {\tt Barrier} with {\tt Allgather} and augment
other collective calls such as {\tt Allreduce} with {\tt Allgather},
using linker wraps and gather the execution times of all processes.
A region is then considered a candidate for frequency scaling if
these three criteria are met:
\begin{itemize}[noitemsep,nolistsep]

\item Total region duration above {\tt THRESHOLD}.

\item Dispersion above {\tt STDEV} of average duration.

\item Skewness below {\tt SKEW}.

\end{itemize}

The numerical values of the parameters above depend on system and
concurrency, e.g.\ dispersion increases with scale.
See Section~\ref{sec:tune} below for a full discussion.

The purpose of clustering is to create larger sections to scale,
and thus save on switching overhead. Clustering for tolerating DVFS
latency was first proposed by Lim et al~\cite{mpicluster}, which
present an algorithm to identify chains of MPI calls that occur close
in time
during execution. 
In our case, to obtain
results in practice we had to develop two extensions to this
algorithm.
The three principle components of our clustering are: 1) collect
regions which each meet the necessary criteria, similar to Lim's
approach; 2) extending these clusters with regions of short duration
(assumes communication is not CPU limited); and 3) accepting a small
loss of performance to cover a gap between clusters when switching
would be even more expensive (``super-clusters").

With reference to
Algorithm~\ref{alg:clustering} it can be seen that attempts to build
clusters are the normal case.  Cluster building resets if a long
region that does not meet the criteria is encountered, but does not
stop.  Once a cluster reaches {\tt THRESHOLD}, it is marked for
scaling, which will happen on the next repetition of the context that
started the cluster.
Any region with a duration less than {\tt SHORT} will always be
accepted into the current cluster, because of the assumption that it
is dominated by communication.
Simply setting {\tt SHORT} to $0$ will remove this clustering feature
from the algorithm.
Finally, a {\tt BRIDGE} cutoff is used to allow clusters to combine to
super-clusters, where the cost of the bridge is preferable compared to
the cost of switching frequencies twice.  A bridge is built until a
new bona fide cluster forms, or if it gets too large, at which point
the algorithm resets.
Super-clustering can be switched off, by setting {\tt BRIDGE} to $0$.

\parah{Frequency Selection} For each cluster we want to determine
a global frequency assignment to adjust all cores.

\label{sec:online_algorithm}

In the online analysis we execute the instrumented program and
data is collected at each collective operation, then clustering runs and
candidate clusters are created.
On the second occurrence of a candidate, the algorithm attempts a
different global frequency. As learning needs to be short for
practical reasons, we use only two target frequencies. We try each
frequency only once per cluster.
The algorithm has a safety valve, tracking the maximum duration
per context where DVFS is applied.
If the scaling causes unwarranted slow-down (determined by the ratio of
the frequencies plus a margin) on the slowest rank compared to the
unscaled iteration, the region is reverted if it is above the
{\tt BRIDGE} threshold, or re-entered into the decision process if not.
All processes have the same data, so reach the same conclusion.
If the decision gets reverted, it applies to the next occurrence.

Putting a limit on the number of trial frequencies is motivated by
several factors. First, the number of dynamic repetitions of a context
may be small, e.g.\ at most ten repetitions in NWChem. Second, as some
trials are bound to degrade performance, fewer trials eliminate
potential slowdown.
Finally, the region selection criteria and acceptable slowdown are
not independent.
The actual frequency values are quantified in Section~\ref{sec:tune}.

%WLAV: placed here purely to make Latex put it on the left column
\begin{algorithm}[t]
\tiny %scriptsize
\DontPrintSemicolon
\SetAlgoLined
\KwIn{$context$, $region$, and $is\_noisy$}
\KwResult{program state update}\;
\SetKwFunction{KwFn}{Fn}
\tcc{{\it decisions} is a map of <context, DVFS decision>; program
{\it State} is
either CLUSTERING, SCALING, or BRIDGING; SHORT, STDEV, SKEW, and BRIDGE
are tunable (see text)}\;

\uIf{$is\_noisy$} {
   \tcc{collect this region into a cluster}
   \Call{collect}{$context$, $region$}\;
   \Return\;
} \uElse {
   \tcc{reject, unless very short, or not too long and in between clusters}
   \uIf{{\tt SHORT} $< region$} {
      \uIf{State == {\tt SCALING}} {
         \tcc{potential end of previous cluster, start of a new bridge}
         $cluster\gets 0$\;
         $State\gets$ {\tt BRIDGING}\;
      }
      \uIf{State == {\tt BRIDGING}} {
         \Call{bridging}{$context$, $region$}\;
         \Return\;
      }
      \Call{reset\_local\_state}{\,}\;
   } \uElse {
       \tcc{short region: communication dominates, collect anyway}
       \Call{collect}{$context$, $region$}\;
   }
   \Return\;
}\;

% WLAV: for the life of me, I can't get \Procedure to work :P
\MyRoutine{{\sc collect}($context$, $region$)} {
   \uIf{State == {\tt BRIDGING}} {
      \tcc{(potential) start of a new cluster}
      $cluster\gets 0$\;
      $State\gets$ {\tt CLUSTERING}\;
   }
   \Switch{State} {
      \Case{\tt CLUSTERING} {
         $cluster\gets$ \Call{sum}{$cluster$, $region$}\;
         $local\_decisons[context]$ = {\tt SCALE}\;
         \uIf{{\tt THRESHOLD} $< cluster$} {
            \tcc{cluster has grown large enough}
            $State\gets$ {\tt SCALING}\;
            \Call{scaling}{$context$, $region$}\;
         }
      }
      \Case{\tt SCALING} {
         \Call{scaling}{$context$, $region$}\;
      }
   }
}\;

\MyRoutine{{\sc scaling}($context$, $region$)} {
   \uIf{$pending\_decisions$} {
      $decisions.update(local\_decisions)$\;
      $decisions.update(bridge\_decisions)$\;
      \Call{reset\_local\_state}{\,}\;
   }
   $decisons[context]$ = {\tt SCALE}\;
}\;

\MyRoutine{{\sc bridging}($context$, $region$)} {
    $cluster\gets$ \Call{sum}{$cluster$, $region$}\;
    $bridge\_decisons[context]$ = {\tt SCALE}\;
    \uIf{{\tt BRIDGE} $< cluster$} {
       \tcc{distance from last cluster has grown too large}
       \Call{reset\_local\_state}{\,}\;
    }
}
\caption{Clustering of regions}
\label{alg:clustering}
\end{algorithm}

%Figure~\ref{fig:design} shows the basic workflow of our online clustering algorithm. When it comes to a new region, the first step is to calculate its context key and check if it already exists in the history context key list. If not, then it means that this region is new, the system should be switched to the high power state and execute. If yes, then it means that this region is repeated, the system should be switched to the low power state and execute.  Let's suppose it is a new region, then it goes to the training component. If it is less than \emph{short\_threshold}, it will be accumulated in the `current list', or in another work clustered, until the total length is larger than \emph{cluster\_threshold}, and the same region occurs \emph{k} times. Then this region is recorded into the `history list'. However, if this new region is larger than \emph{short\_threshold}, it means that a large new region is in the way and the current list is not longer enough to be recorded, so the new region has to be disregarded, and the current list has to be cleared. 


While the online analysis is concurrency and input independent, the
offline analysis gives us an indication of how much optimization is
unexploited for a given problem (fixed concurrency and input), when
comparing to online.  Here, we execute the program once for each
discrete frequency level available for DVFS and collect traces.  A
trace analysis combines all experiments, forms clusters and selects the optimal frequency
assignment: this can be any of the available frequencies on the
system. Thus, contexts that occur only once, most importantly
initialization, and the first iteration of a repeating context can
scale when re-executing the program.  Further, the offline analysis
can select the most energy-optimal candidate, given a constraint on
performance loss, from a static set of frequencies.

When using the offline analysis, the optimized execution does not
augment collectives with  {\tt Allgather}, resulting in faster
execution. Note that the scalability of  {\tt Allgather} is not a real
concern for the online analysis: we can revert to the original
collective after making a decision, but in doing so give up the safety valve.
In our experiments, reversals do not happen after the
first successful scaling, so we could revert to the original collective on
the third occurrence.
Furthermore, changes in collectives can only have a measurable
effect over the full run, if the original collectives consumed a significant
portion of the overall running time.
If so, reversals are unlikely to happen.
